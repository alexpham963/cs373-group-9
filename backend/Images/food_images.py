import requests
from bs4 import BeautifulSoup
from PIL import Image

file = open("backend/FoodOrgs/FoodOrg4.txt")
links = open("backend/Images/FoodOrgImages.txt", "w")
counter = 0

for line in file:

    # split line into a search phrase
    info = line.split(",")
    search = info[0] + " " + info[1] + " " + info[2] + " " + info[3]

    url = "https://www.google.com/search?tbm=isch&q=" + search
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")

    # get the first image
    img = soup.find_all("img")[1]
    links.write(img["src"] + "\n")

    # save image as png
    img_url = img["src"]
    img = Image.open(requests.get(img_url, stream=True).raw)
    img.save("backend/Images/FoodOrgImages2/food_org_img" + str(counter) + ".png")

    counter += 1

file.close()
links.close()
