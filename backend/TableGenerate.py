from Models import (
    app,
    db,
    FoodOrg,
    County,
    GroceryStore,
    FoodOrg_County,
    FoodOrg_GroceryStore,
)


def refresh_tables():
    # Drop all existing tables so we can make new ones
    print("dropping tables")
    db.drop_all()

    # Create the tables in the database
    print("creating new tables")
    db.create_all()


def add_grocerystores():
    grocerystores = []
    with open("./GroceryStores/GroceryStore.txt", "r") as groceryData, open(
        "./GroceryStores/snap_website.txt", "r"
    ) as snap_web:
        id_counter = 1

        snapweb_dict = dict()
        for line in snap_web:
            values = line.strip().split(",")
            snapweb_dict[values[0].lower()] = values[1]

        for line in groceryData:
            values = line.strip().split(",")
            store_name = (
                values[0].split(" ")[0].lower()
            )  # Split on space and take the first part
            snap_web_result = snapweb_dict.get(store_name, "None")

            grocerystore = GroceryStore(
                id=id_counter,
                name=values[0],
                city=values[2],
                address=values[3],
                zip_code=values[4],
                county_string=values[1],
                is_snap=snap_web_result != "None",
                snap_web=snap_web_result,
            )
            grocerystores.append(grocerystore)
            id_counter += 1
    return grocerystores


def add_foodorgs():
    foodorgs = []
    with open("./FoodOrgs/FoodOrg.txt", "r") as foodOrgData:
        id_counter = 1
        for line in foodOrgData:
            values = line.strip().split(",")
            foodorg = FoodOrg(
                id=id_counter,
                county=values[0],
                name=values[1],
                address=values[2],
                city=values[3],
                zip_code=values[4],
                phone_number=values[5],
                website=values[6],
                is_bank="bank" in values[1].lower(),
                pantry="pantry" in values[1].lower(),
                food_kitchen="kitchen" in values[1].lower(),
            )
            foodorgs.append(foodorg)
            id_counter += 1
    return foodorgs


def add_counties():
    counties = []  # List to store created County instances
    with open("./GroceryStores/GroceryStore.txt", "r") as groceryFile, open(
        "./Counties/CountyInfo.txt", "r"
    ) as countyFile, open(
        "./Counties/CountyPopulation.txt", "r"
    ) as supplementalData, open(
        "./FoodOrgs/FoodOrg.txt", "r"
    ) as foodOrgFile, open(
        "./Counties/CountyWebsite.txt"
    ) as websiteFile:
        # skip column name row:
        next(countyFile)
        next(supplementalData)
        next(groceryFile)

        # iterate through countyFiles, obtain data to connect instances
        for line in countyFile:
            countyDataList = line.strip().split(",")
            countMoreDataLine = supplementalData.readline()
            countMoreDataList = countMoreDataLine.strip().split(",")
            websiteLine = websiteFile.readline()
            websiteLine = websiteLine.strip()
            # Assuming the County constructor matches the order and type of these values
            county = County(
                id=countMoreDataList[1],
                name=countyDataList[0],
                total_population=int(countMoreDataList[2]),
                percent_people_insecure=float(countyDataList[2]),
                num_people_insecure=int(countyDataList[3]),
                percent_children_insecure=float(countyDataList[4]),
                num_children_insecure=int(countyDataList[5]),
                cost_per_meal=float(countyDataList[6]),
                county_web=websiteLine,
            )
            # Add the created County instance to the list
            counties.append(county)
    # Return the list of County instances
    return counties


def add_data():
    grocery_list = add_grocerystores()
    food_org_list = add_foodorgs()
    county_list = add_counties()

    # Convert county names into ids for grocery store
    # Creates associations for foreign keys
    for g in grocery_list:
        county_string = g.county_id
        county_id = -1
        for c in county_list:
            if c.name == county_string:
                county_id = c.id
        g.county_id = county_id

    # Associate data in the association tables
    foodorg_county_list = []
    for f in food_org_list:

        for c in county_list:
            if f.county == c.name:
                foodorg_county = FoodOrg_County(foodorg_id=f.id, county_id=c.id)
                foodorg_county_list.append(foodorg_county)

    foodorg_grocerystore_list = []
    for c in county_list:
        for g in grocery_list:
            if g.county_id == c.id:
                for f in food_org_list:
                    if c.name == f.county:
                        foodorg_grocerystore = FoodOrg_GroceryStore(
                            foodorg_id=f.id, grocerystore_id=g.id
                        )
                        foodorg_grocerystore_list.append(foodorg_grocerystore)

    # add data to the database
    db.session.add_all(grocery_list)
    db.session.add_all(food_org_list)
    db.session.add_all(county_list)
    db.session.add_all(foodorg_county_list)
    db.session.add_all(foodorg_grocerystore_list)
    db.session.commit()
    pass


with app.app_context():
    refresh_tables()
    add_data()
#     pass
