# Texas Food Insecurity
**Website Link:** https://txfoodinsecurity.me or https://texasfoodinsecurity.me  
**Postman API Documentation:** https://documenter.getpostman.com/view/32886907/2sA2r535HM  
**Presentation Link:** https://www.youtube.com/watch?v=8wI39b4t8ws  
**API Link:** https://api.texasfoodinsecurity.me  
**Pipeline Link:** https://gitlab.com/alexpham963/cs373-group-9/-/pipelines  

## Group Number:  9
| Team Member | GitLabID    | EID         |
| :---------- | :---------- | :---------- |
| Alex Pham | @alexpham963 | app2345 |
| Aditya Jagarlamudi | @adityajagarlamudi  | aj34468 |
| Eric Rodriguez | @Keeylo | er34355 |
| Maxwell Wu | @25wum | mmw3567 |
| Pascal Garcia | @garcia_pascal | prg754 |  

## GitSha
| Phase # | GitSha ID|
| :------ | :----- |
| Phase 1 | faf3dc6676df6a832b75ef1770be3dd520c38808 |
| Phase 2 | 785d0f9afb4de42e66867eef6954132aea7e4e81 |
| Phase 3 | 82c87f54cdbc3b0a63a4fcef65fd3f72d65e4992 |
| Phase 4 | b995058fa922f6bb1418837241dd62b724825670 |

## Phase Leaders
| Phase # | Leader | Responsibilities |
| :------ | :----- | :----- |
| Phase 1 | Alex Pham | Organize group, plan meetings, design and supervise frontend development |
| Phase 2 | Pascal Garcia | Organize group, plan meetings, design and supervise backend and database development |
| Phase 3 | Aditya Jagarlamudi | Organize group, plan meetings, supervise data scraping and testing |
| Phase 4 | Maxwell Wu | Organize group, plan meetings, supervise refactoring and implementatin of visualizations |

## Phase 1: Estimated and Actual Completion Time
| Team Member | Estimated (hours) | Actual (hours) |
| :---------- | :---------- | :---------- |
| Alex Pham | 10 | 20 |
| Aditya Jagarlamudi | 8 | 10 |
| Eric Rodriguez | 13 | 18 |
| Maxwell Wu | 7 | 10 |
| Pascal Garcia | 10 | 20 |  

## Phase 2: Estimated and Actual Completion Time
| Team Member | Estimated (hours) | Actual (hours) |
| :---------- | :---------- | :---------- |
| Alex Pham | 30 | 50 |
| Aditya Jagarlamudi | 20 | 40 |
| Eric Rodriguez | 20 | 35 |
| Maxwell Wu | 30 | 40 |
| Pascal Garcia | 30 | 40 |  

## Phase 3: Estimated and Actual Completion Time
| Team Member | Estimated (hours) | Actual (hours) |
| :---------- | :---------- | :---------- |
| Alex Pham | 10 | 15 |
| Aditya Jagarlamudi | 15 | 22 |
| Eric Rodriguez | 15 | 27 |
| Maxwell Wu | 10 | 10 |
| Pascal Garcia | 8 | 12 |

## Phase 4: Estimated and Actual Completion Time
| Team Member | Estimated (hours) | Actual (hours) |
| :---------- | :---------- | :---------- |
| Alex Pham | 20 | 38 |
| Aditya Jagarlamudi | 10 | 12 |
| Eric Rodriguez | 8 | 14 |
| Maxwell Wu | 10 | 10 |
| Pascal Garcia | 5 | 8 |

Comments:
gitlab-ci.yml, acceptance test from MetroNYC to get chrome driver to work in pipelines
