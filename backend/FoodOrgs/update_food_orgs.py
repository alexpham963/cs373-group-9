food_orgs3 = open("backend/FoodOrgs/FoodOrg3.txt", "r")
websites = open("backend/FoodOrgs/Websites.txt", "r")
food_orgs4 = open("backend/FoodOrgs/FoodOrg4.txt", "w")

sites = []
for line in websites:
    sites.append(line.strip())

lines = []
for line in food_orgs3:
    lines.append(line.strip())

for i in range(len(sites)):
    new_line = lines[i] + "," + sites[i] + "\n"
    food_orgs4.write(new_line)

food_orgs4.close()
food_orgs3.close()
websites.close()
