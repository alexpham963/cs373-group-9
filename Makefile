docker-build:
	docker compose build

docker-up:
	docker compose up

frontend-start:
	cd ./frontend && \
	yarn start

backend-start:
	cd ./backend && \
	python3 wsgi.py

database-setup:
	cd ./backend && \
	python3 TableGenerate.py

environment:
	python3.10 -m venv venv

venv-install:
	cd ./frontend && \
	pip install -r requirements.txt && \
	cd ../backend && \
	pip install -r requirements.txt
