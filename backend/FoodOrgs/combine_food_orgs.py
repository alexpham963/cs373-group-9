counties = open("backend/Counties/CountyInfo.txt", "r")
combined = open("backend/FoodOrgs/CombinedBanksandOrgs2.txt", "w")

# county, name of food org/bank, address of bank/org,
counter = 0
for line in counties:
    if counter > 0:

        county = line.split(",")[0]
        in_orgs = False
        banks = open("backend/FoodOrgs/FullBank.txt", "r")
        orgs = open("backend/FoodOrgs/FoodOrg2.txt", "r")

        for line2 in orgs:
            if county in line2.split(",")[6].strip():
                cutoff = 2 + len(line2.split(",")[6]) + len(line2.split(",")[5])
                combined.write(county + "," + line2[: len(line2) - cutoff])
                combined.write("\n")
                in_orgs = True

        if in_orgs == False:
            for line3 in banks:
                if county.lower() in line3:
                    combined.write(line3)
                    break

    counter += 1
