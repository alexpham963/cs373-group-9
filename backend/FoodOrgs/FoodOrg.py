from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
import time

# Set up and get web page
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
url = "https://www.homelessshelterdirectory.org/foodbanks/state/texas"
driver.get(url)

# get links to all cities
time.sleep(2)
num_results = []
links = []
x_path_string = "/html/body/div[1]/section/div/div[1]/div[1]/div/div/table/tbody/tr["
x_path_string2 = "/html/body/div[1]/section/div/div/div[1]/div/div/table/tbody/tr["
for i in range(2, 118):

    # get and store link
    loc = x_path_string + str(i) + "]/td[1]/a"
    table_row = driver.find_element(By.XPATH, loc)
    links.append(table_row.get_attribute("href"))

    # get and store number of results
    loc2 = x_path_string2 + str(i) + "]/td[2]"
    table_data = driver.find_element(By.XPATH, loc2)
    num_result = int(table_data.text.strip())
    num_results.append(num_result)


# for each city, get the address, zip code, phone number, and website

for i in range(len(links)):

    link = links[i]
    num_result = num_results[i]

    for j in range(3, num_result + 3):

        if j > 12:
            break

        driver.get(link)
        time.sleep(1)

        # go to the org page
        xpath = (
            "/html/body/div[1]/section/div/div/div[1]/div/div/div["
            + str(j)
            + "]/div[2]/h4/a"
        )
        org = driver.find_element(By.XPATH, xpath)
        name = org.get_attribute("text")
        name = name.replace(",", " ")
        sublink = org.get_attribute("href")

        driver.get(sublink)
        time.sleep(1)
        # get the address, city, and zip code
        xpath = "/html/body/div[1]/section/div/div/div[1]/article/div[3]/div/div[1]/p"
        addr_info = driver.find_element(By.XPATH, xpath).text
        addr_split = addr_info.split("\n")

        addr = addr_split[1]
        addr = addr.replace(",", " ")
        city = addr_split[2].split(", ")[0]
        zip = addr_split[2].split("- ")[1]

        # get the phone number
        xpath = (
            "/html/body/div[1]/section/div/div/div[1]/article/div[3]/div/div[2]/p/a[1]"
        )
        contact = driver.find_element(By.XPATH, xpath).text

        # get website link
        site = ""
        xpath = (
            "/html/body/div[1]/section/div/div/div[1]/article/div[3]/div/div[2]/p/a[2]"
        )
        try:
            site = driver.find_element(By.XPATH, xpath).get_attribute("href")
        except NoSuchElementException:
            site = "N/A"

        print(name + "," + addr + "," + city + "," + zip + "," + contact + "," + site)
