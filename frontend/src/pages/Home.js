import React, { useState, useEffect} from "react";
import "../index.css";
import "../styles/Home.css";

import charity_1 from "../images/home_images/charity_1.avif";
import charity_2 from "../images/home_images/charity_2.jpg";
import charity_3 from "../images/home_images/charity_3.jpg";
import charity_4 from "../images/home_images/charity_4.jpg";
import charity_5 from "../images/home_images/charity_5.jpg";

const Home = () => {
  const slideshow = [charity_1, charity_2, charity_3, charity_4, charity_5];

  const [currentSlide, setCurrentSlide] = useState(0);
  const [activeFAQ, setActiveFAQ] = useState(null);

  const faqData = [
    {
      id: 1,
      question: "What is food insecurity, and how is it defined?",
      answer:
        "Food insecurity refers to the lack of consistent access to enough food for an active, healthy life due to financial constraints. It's often characterized by uncertainty about where the next meal will come from.",
    },
    {
      id: 2,
      question:
        "How does food insecurity exacerbate the challenges faced by individuals with food allergies?",
      answer:
        "Food insecurity exacerbates the challenges for individuals with food allergies by limiting access to safe and nutritious food options. Those facing food insecurity often have to prioritize affordability over allergen-free choices, increasing the risk of allergic reactions. Limited availability of safe foods in assistance programs further complicates managing food allergies on a restricted budget. Addressing this issue requires targeted efforts to ensure the availability of affordable, allergen-free food options, thus supporting the health and safety of individuals with food allergies amidst food insecurity.",
    },
    {
      id: 3,
      question: "How prevalent is food insecurity globally and locally?",
      answer:
        "Food insecurity affects millions of people worldwide, with varying degrees of severity in different regions. Locally, the prevalence depends on factors such as economic conditions, social policies, and community resources.",
    },
    {
      id: 4,
      question: "What are the main causes of food insecurity?",
      answer:
        "The main causes include poverty, unemployment, low wages, high living costs, lack of access to nutritious food, inadequate social safety nets, and systemic inequalities.",
    },
    {
      id: 5,
      question: "Who is most affected by food insecurity?",
      answer:
        "Vulnerable populations such as children, seniors, low-income households, minority communities, individuals with disabilities, and those living in rural or urban food deserts are disproportionately affected.",
    },
    {
      id: 6,
      question:
        "What are the consequences of food insecurity on individuals and communities?",
      answer:
        "Consequences include malnutrition, poor physical and mental health, developmental delays in children, reduced productivity, increased healthcare costs, and social isolation.",
    },
    {
      id: 7,
      question:
        "How does food insecurity impact children and their development?",
      answer:
        "Food insecurity can hinder children's physical growth, cognitive development, academic performance, and overall well-being, leading to long-term consequences for their future health and success.",
    },
    {
      id: 8,
      question:
        "What are the signs that someone may be experiencing food insecurity?",
      answer:
        "Signs include skipping meals, eating less, reliance on low-cost, low-nutrient foods, anxiety about food availability, and difficulty concentrating due to hunger.",
    },
    {
      id: 9,
      question: "How can I help someone who is facing food insecurity?",
      answer:
        "You can help by donating food, volunteering at food banks or soup kitchens, advocating for policy changes, supporting community programs, and spreading awareness about food insecurity.",
    },
    {
      id: 10,
      question:
        "What resources are available for individuals and families experiencing food insecurity?",
      answer:
        "Resources include food banks, soup kitchens, government assistance programs (e.g., SNAP), community gardens, meal delivery services, and local nonprofit organizations.",
    },
    {
      id: 11,
      question: "How do food banks and food assistance programs work?",
      answer:
        "Food banks collect, store, and distribute donated food to individuals and families in need through partnerships with local agencies, nonprofits, and volunteers.",
    },
    {
      id: 12,
      question: "How can I donate to or volunteer with local food banks?",
      answer:
        "Contact your local food bank to inquire about donation guidelines, volunteer opportunities, and other ways to support their mission.",
    },
  ];

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentSlide((currentSlide) => (currentSlide + 1) % slideshow.length);
    }, 5000);
    return () => clearInterval(intervalId);
  }, [slideshow.length]);

  return (
    <>
      <div className="page-background">
        <div
          className="slideshow"
          style={{
            backgroundImage: `url(${slideshow[currentSlide]})`,
            minHeight: "100%",
          }}
        >
          <div className="slideshowbox">
            <h1>Texas Food Insecurity</h1>
            <p>
              To serve as a crucial resource for individuals in Texas facing
              food insecurity, facilitating easier access to food assistance
              services and raising awareness about the underlying factors
              contributing to food insecurity in Texas.
            </p>
          </div>
        </div>
        <div className="center-text">
          <h1>What is Food Insecurity? A Growing Concern In Texas?</h1>
          <iframe
            allowFullScreen={true}
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            referrerPolicy="strict-origin-when-cross-origin"
            title="What is Food Insecurity"
            width="640"
            height="360"
            src="https://www.youtube.com/embed/2YgxG6tyaXs?enablejsapi=1&amp;origin=https%3A%2F%2Fwww.texasfoodinsecurity.me&amp;widgetid=3" //www.youtube.com/watch?v=2YgxG6tyaXs
            id="widget4"
          ></iframe>
          <iframe
            allowFullScreen={true}
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            referrerPolicy="strict-origin-when-cross-origin"
            title="A Growing Concern in Texas"
            width="640"
            height="360"
            src="https://www.youtube.com/embed/yP5VKVLKWx0?enablejsapi=1&amp;origin=https%3A%2F%2Fwww.texasfoodinsecurity.me&amp;widgetid=3" //www.youtube.com/watch?v=2YgxG6tyaXs
            id="widget4"
          ></iframe>
        </div>
        {/* <div className="section-container">
          <h1>test</h1>
        </div> */}
        {/* <iframe
          src="https://www.arcgis.com/apps/webappviewer/index.html?id=15e1c457b56c4a729861d015cd626a23"
          style={{ width: "100%", height: "450px", border: 0 }}
          allowFullScreen={true}
          loading={"lazy"}
          referrerPolicy={"no-referrer-when-downgrade"}
        ></iframe> */}
        <div className="center-text">
          <h1 style={{ padding: "2%" }}>Food Insecurity Facts About Texas</h1>
          <ul style={{ textAlign: "left", marginLeft: "10%", width: "80%" }}>
            <li>
              1 in 7 Texans experiences food insecurity, totaling over 4 million
              individuals.
            </li>
            <li>
              Rural areas often face higher rates of food insecurity due to
              limited access to grocery stores and other food resources.
            </li>
            <li>
              Food insecurity disproportionately affects minority communities,
              including Hispanic and Black populations, in Texas.
            </li>
            <li>
              Despite being one of the nation's top agricultural producers,
              Texas struggles with food insecurity due to income inequality and
              poverty rates.
            </li>
            <li>
              Food insecurity has negative impacts on health, education, and
              economic productivity in Texas communities.
            </li>
            <li>
              Efforts by food banks, nonprofits, and government agencies aim to
              address food insecurity through various programs and initiatives
              across the state.
            </li>
          </ul>
        </div>
        <div className="center-text">
          <h1 style={{ padding: "2%" }}>
            Underlying Issues to Food Insecurity in Texas
          </h1>
          <ul style={{ textAlign: "left", marginLeft: "10%", width: "80%" }}>
            <p>
              Food insecurity is a significant issue in Texas, impacting
              individuals and families across the state. Below are four key
              points that highlight the causes and implications of this problem:
            </p>
            <li>
              <strong>High Poverty Rates:</strong> Higher poverty rates in Texas
              correlate with higher levels of food insecurity, making it
              difficult for many residents to access sufficient, nutritious food
              regularly.
            </li>
            <li>
              <strong>Large Rural Areas with Limited Access:</strong> Vast rural
              areas in Texas often lack close proximity to grocery stores and
              fresh food markets, creating "food deserts" where it's challenging
              to purchase affordable and healthy food.
            </li>
            <li>
              <strong>Impact of Economic Fluctuations and Disasters:</strong>{" "}
              Economic downturns, job losses, and natural disasters can disrupt
              food supply chains and increase food insecurity in Texas.
            </li>
            <li>
              <strong>Inadequate Public Assistance:</strong> While assistance
              programs exist, they may not fully meet the needs of all families
              due to eligibility requirements, bureaucratic hurdles, or
              insufficient funding and resources.
            </li>
          </ul>
        </div>
        <div className="center-text">
          <h1 style={{ padding: "2%" }}>Stories to Hear</h1>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                width: "30%",
              }}
            >
              <a
                href="https://www.storybanktexas.org/2017/11/marissa/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  className="story-image"
                  src="https://www.feedingtexas.org/wp-content/uploads/2019/06/2017_2_54.Photo_.1.Bryan_.Rodriguez.Marissa.Martinez.CTFB_-e1561668766362-600x600.jpg"
                  alt="Marissa"
                  style={{ width: "100%", padding: "5%" }}
                />
                <p
                  style={{
                    display: "inline-block",
                    backgroundColor: "var(--section-color-2)",
                    color: "var(--button-color)",
                    padding: "10px 20px",
                    textDecoration: "none",
                    borderRadius: "4px",
                    textAlign: "center",
                    fontWeight: "bold",
                  }}
                >
                  Marissa
                </p>
              </a>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                width: "30%",
              }}
            >
              <a
                href="https://www.storybanktexas.org/2017/12/nancy-luana/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  className="story-image"
                  src="https://www.feedingtexas.org/wp-content/uploads/2019/06/2017_2_63.Photo_.2.Nancy_.Rodriguez.Luana_.Rodriguez.CTFB_-e1561668399176-600x600.jpg"
                  alt="Nancy & Luana"
                  style={{ width: "100%", padding: "5%" }}
                />
                <p
                  style={{
                    display: "inline-block",
                    backgroundColor: "var(--section-color-2)",
                    color: "var(--button-color)",
                    padding: "10px 20px",
                    textDecoration: "none",
                    borderRadius: "4px",
                    textAlign: "center",
                    fontWeight: "bold",
                  }}
                >
                  Nancy & Luana
                </p>
              </a>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                width: "30%",
              }}
            >
              <a
                href="https://www.storybanktexas.org/2018/08/kemi/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  className="story-image"
                  src="https://www.feedingtexas.org/wp-content/uploads/2019/06/2018_2_24.Photo_.1.Oluwakemi.Odedoji.CTFB_-600x600.jpg"
                  alt="Kemi"
                  style={{ width: "100%", padding: "5%" }}
                />
                <p
                  style={{
                    display: "inline-block",
                    backgroundColor: "var(--section-color-2)",
                    color: "var(--button-color)",
                    padding: "10px 20px",
                    textDecoration: "none",
                    borderRadius: "4px",
                    textAlign: "center",
                    fontWeight: "bold",
                  }}
                >
                  Kemi
                </p>
              </a>
            </div>
          </div>
        </div>
        <div className="center-text" style={{ width: "60%" }}>
          <h1 style={{ padding: "2%" }}>Frequently Asked Questions</h1>
        </div>
        <div>
          {faqData.map((faq) => (
            <div key={faq.id} className="faq-item" style={{ color: "d8a51a" }}>
              <button
                className="faq-question"
                onClick={() =>
                  setActiveFAQ(activeFAQ === faq.id ? null : faq.id)
                }
                style={{ width: "60%", marginLeft: "20%" }}
              >
                {faq.question}
              </button>
              <div
                className={`faq-answer ${activeFAQ === faq.id ? "active" : ""}`}
                style={{
                  display: activeFAQ === faq.id ? "block" : "none",
                  width: "60%",
                  marginLeft: "20%",
                }}
              >
                {faq.answer}
              </div>
            </div>
          ))}
        </div>{" "}
      </div>
    </>
  );
};

export default Home;
