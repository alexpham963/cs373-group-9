from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from sqlalchemy import ForeignKey

app = Flask(__name__)
CORS(app)

with open("./DatabaseCred/database-username.txt", "r") as file, open(
    "./DatabaseCred/database-password.txt", "r"
) as file2, open("./DatabaseCred/database-endpoint.txt", "r") as file3:
    username = file.read().strip()
    password = file2.read().strip()
    endpoint = file3.read().strip()


postgresConnection = f"postgresql://{username}:{password}@{endpoint}"

app.config["SQLALCHEMY_DATABASE_URI"] = postgresConnection

db = SQLAlchemy(app)
ma = Marshmallow(app)


# Main model definitions for our data
class FoodOrg(db.Model):
    __tablename__ = "foodorg"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zip_code = db.Column(db.Integer)
    phone_number = db.Column(db.String)
    county = db.Column(db.String)
    website = db.Column(db.String)
    is_bank = db.Column(db.Boolean)
    pantry = db.Column(db.Boolean)
    food_kitchen = db.Column(db.Boolean)

    def __init__(
        self,
        id,
        name,
        address,
        city,
        zip_code,
        phone_number,
        county,
        website,
        is_bank,
        pantry,
        food_kitchen,
    ):
        self.id = id
        self.name = name
        self.address = address
        self.city = city
        self.zip_code = zip_code
        self.phone_number = phone_number
        self.county = county
        self.website = website
        self.is_bank = is_bank
        self.pantry = pantry
        self.food_kitchen = food_kitchen

    def __str__(self):
        return self.name


class County(db.Model):
    __tablename__ = "county"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    total_population = db.Column(db.Integer)
    percent_people_insecure = db.Column(db.Float)
    num_people_insecure = db.Column(db.Integer)
    percent_children_insecure = db.Column(db.Float)
    num_children_insecure = db.Column(db.Integer)
    cost_per_meal = db.Column(db.Float)
    county_web = db.Column(db.String)

    def __init__(
        self,
        id,
        name,
        total_population,
        percent_people_insecure,
        num_people_insecure,
        percent_children_insecure,
        num_children_insecure,
        cost_per_meal,
        county_web,
    ):
        self.id = id
        self.name = name
        self.total_population = total_population
        self.percent_people_insecure = percent_people_insecure
        self.num_people_insecure = num_people_insecure
        self.percent_children_insecure = percent_children_insecure
        self.num_children_insecure = num_children_insecure
        self.cost_per_meal = cost_per_meal
        self.county_web = county_web

    def __str__(self):
        return self.name


class GroceryStore(db.Model):
    __tablename__ = "grocerystore"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    city = db.Column(db.String)
    address = db.Column(db.String)
    zip_code = db.Column(db.Integer)
    county_id = db.Column(db.Integer)
    is_snap = db.Column(db.Boolean)
    snap_web = db.Column(db.String)

    def __init__(
        self, id, name, city, address, zip_code, county_string, is_snap, snap_web
    ):
        self.id = id
        self.name = name
        self.city = city
        self.address = address
        self.zip_code = zip_code
        self.county_id = county_string
        self.snap_web = snap_web
        self.is_snap = is_snap

    def __str__(self):
        return self.name


# Association Tables used for many to many relationships
class FoodOrg_GroceryStore(db.Model):
    __tablename__ = "foodorg_grocerystore"
    foodorg_id = db.Column(db.Integer, primary_key=True)
    grocerystore_id = db.Column(db.Integer, primary_key=True)

    def __init__(self, foodorg_id, grocerystore_id):
        self.foodorg_id = foodorg_id
        self.grocerystore_id = grocerystore_id

    def __str__(self):
        return self.name


class FoodOrg_County(db.Model):
    __tablename__ = "foodorg_county"
    foodorg_id = db.Column(db.Integer, primary_key=True)
    county_id = db.Column(db.Integer, primary_key=True)

    def __init__(self, foodorg_id, county_id):
        self.foodorg_id = foodorg_id
        self.county_id = county_id

    def __str__(self):
        return self.name


# used for json serialization
class FoodOrgSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = FoodOrg


class CountySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = County


class GroceryStoreSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = GroceryStore


class FoodOrg_GroceryStoreSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = FoodOrg_GroceryStore


class FoodOrg_CountySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = FoodOrg_County
