import requests
from bs4 import BeautifulSoup


def get_link(line):

    org_name = line.split(",")[1].split(" ")
    org_name = [word for word in org_name if (word != "" and word != " ")]

    url = "https://www.homelessshelterdirectory.org/foodbank/tx_"
    for word in org_name:
        url += word.lower() + "-"
    url = url[: len(url) - 1]

    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    links = soup.find_all("a", class_="btn btn_blue")

    for link in links:
        if "www" in link["href"]:
            return link["href"]

    return url


orgs = open("backend/FoodOrgs/FoodOrg3.txt", "r")
websites = open("backend/FoodOrgs/Websites.txt", "w")

for line in orgs:
    if "Food Bank" in line:
        org_name = line.split(",")[1]
        if "Houston" in org_name:
            websites.write("https://www.houstonfoodbank.org/")
        elif "South Plains" in org_name:
            websites.write("https://www.spfb.org/")
        elif "Central Texas" in org_name:
            websites.write("https://www.centraltexasfoodbank.org/")
        elif "Coastal Bend" in org_name:
            websites.write("https://coastalbendfoodbank.org/")
        elif "Tarrant" in org_name:
            websites.write("https://tafb.org/")
        elif "Rio Grande" in org_name:
            websites.write("https://foodbankrgv.com/")
        elif "East Texas" in org_name:
            websites.write("https://www.easttexasfoodbank.org/")
        elif "El Pasoans" in org_name:
            websites.write("https://elpasoansfightinghunger.org/")
        elif "Golden Crescent" in org_name:
            websites.write("https://www.tfbgc.org/")
        elif "High Plains" in org_name:
            websites.write("https://www.hpfb.org/")
        elif "North Texas" in org_name:
            websites.write("https://ntfb.org/")
        elif "San Antonio" in org_name:
            websites.write("https://safoodbank.org/")
        elif "South Texas" in org_name:
            websites.write("https://www.southtexasfoodbank.org/")
        elif "Southeast Texas" in org_name:
            websites.write("https://setxfoodbank.org/")
        elif "West Texas" in org_name:
            websites.write("https://wtxfoodbank.org/")
        elif "Wichita Falls" in org_name:
            websites.write("https://www.wfafb.org/")
        elif "West Central" in org_name:
            websites.write("https://fbwct.org/")
        else:
            link = get_link(line)
            websites.write(link)
        websites.write("\n")

    else:
        link = get_link(line)
        websites.write(link + "\n")

orgs.close()
websites.close()
