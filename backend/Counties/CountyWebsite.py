import requests
from bs4 import BeautifulSoup

url = "https://texascountiesdeliver.org/texas-county-websites/"

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3"
}
response = requests.get(url, headers=headers)
response.raise_for_status()

soup = BeautifulSoup(response.text, "html.parser")

county_links = soup.find_all("a", href=True)

links = [
    link["href"]
    for link in county_links
    if "http://" in link["href"] or "https://" in link["href"]
]

for link in links:
    print(link)
