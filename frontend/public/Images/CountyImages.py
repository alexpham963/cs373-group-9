import requests
import io
from bs4 import BeautifulSoup
from PIL import Image

url = "https://en.wikipedia.org/wiki/List_of_counties_in_Texas"
page = requests.get(url)
soup = BeautifulSoup(page.content, "html.parser")

# get all links on wikpedia county page
links = soup.find_all("a")
links = [link.get("href") for link in links]
links = [link for link in links if link != None and "_County,_Texas" in link]
links = list(set(links))
links.sort()

# get rid of old counties that don't exist anymore
defunct = [
    "Buchel",
    "Encinal",
    "Foley",
    "Greer",
    "Perdido",
    "Santa_Fe",
    "Wegefarth",
    "/Worth",
]
defunct_links = []

for link in links:
    for name in defunct:
        if name in link:
            defunct_links.append(link)

links = [link for link in links if link not in defunct_links]

counter = 1
for link in links:
    page = requests.get("https://en.wikipedia.org/" + link)
    soup = BeautifulSoup(page.content, "html.parser")

    # get corresponding image for each county
    img = soup.find_all("img")[3]
    img_url = "https:" + img["src"]
    img_url = img_url.split("/250")[0].replace("/thumb", "")
    print(img_url)

    # save image
    r = requests.get(img_url, stream=True)
    img = Image.open(io.BytesIO(r.content))

    img.save("frontend/public/Images/CountyPictures/county_pic" + str(counter) + ".jpg")

    counter += 1


print(links)
print(len(links))
