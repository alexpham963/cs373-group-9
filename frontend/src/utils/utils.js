import axios from "axios";
import { useState, useEffect } from "react";

export const toTitleCase = (string) => {
    string = string ? string.toString() : "";
    return string.replace(/\w\S*/g, (txt) => {
        return txt.charAt(0).toUpperCase() + txt.substr(1);
    });
}

  const modelAttributes = {
    foodorg: ["name", "address", "city", "county", "zip_code"],
    grocery: ["name", "address", "city", "county", "zip_code"],
    county: [
      "name",
      "cost_per_meal",
      "num_children_insecure",
      "num_people_insecure",
      "percent_children_insecure",
      "percent_people_insecure",
      "total_population",
    ],
  };
export const filterDisplayableAttr = (obj, modelType) => {
    return Object.entries(obj).reduce((accum, [key, val]) => {
        if (key.toLowerCase() !== "image" && !key.includes("id") && key !== 'link') {
            if (modelType === "Charity Organizations") {
                if (modelAttributes["foodorg"].includes(key.toString())) {
                    accum[key] = val;
                }
            } else if (modelType === "Grocery Stores") {
                if (modelAttributes["grocery"].includes(key.toString())) {
                    accum[key] = val;
                }
            } else if (modelType === "Counties") {
                if (modelAttributes["county"].includes(key.toString())) {
                    accum[key] = val;
                }
            } else {
                accum[key] = val;
            }
        }
        return accum;
    }, {})
}

export const getDevData = async (modelType) => {
    let response = {};
    let data = [];
    
    if (modelType === "get-all-thrifts") {
      response = await axios.get(
        "https://api.atxassist.me/get-all-thrifts"
      );
      data = response.data;
    } else if (modelType === "get-all-food-pantries") {
      response = await axios.get(
        "https://api.atxassist.me/get-all-food-pantries"
      );
      data = response.data["data"];
    } else if (modelType === "get-all-affordable-housing") {
      response = await axios.get(
        "https://api.atxassist.me/get-all-affordable-housing"
      );
      data = response.data["data"];
    }
    
    return data;
}

getDevData('get-all-affordable-housing')
  .then(data => {
    // do what ever
  })


export const nameAttrFirst = (obj) => {
    const { name, ...rest } = obj;
    return { name, ...rest };
}

export function isEmpty(value) {
    if (Array.isArray(value)) {
        // Check if it's an array and its length is zero
        return value.length === 0;
    } else if (typeof value === 'object' && value !== null) {
        // Check if it's an object (and not null) and has no keys
        return Object.keys(value).length === 0;
    }
    // Return true for null or undefined, false otherwise
    return value == null;
}

export function getOriginFromUrl(url) {
    try {
        const parsedUrl = new URL(url);
        return parsedUrl.origin;
    } catch (error) {
        console.error("Invalid URL:", error);
        return null;
    }
}

export const getModelData = async (modelType) => {
    let response = {};
    let data = [];
    if (!modelType) {
        return data;
    }
    if (modelType.toLowerCase().startsWith("charity")) {
      response = await axios.get(
        "https://api.texasfoodinsecurity.me/foodorg?page_size=1000"
      );
      data = response.data["foodorg"];

      // remove duplicates
      const seenNames = new Set();
      let filteredData = [];
      for (let i = data.length - 1; i >= 0; i--) {
        if (seenNames.has(data[i].name)) {
        } else {
          seenNames.add(data[i].name);
          filteredData.push(data[i]);
        }
      }
      data = filteredData.reverse();
      for (let i = 0; i < data.length; i++) {
        data[i]["Image"] = `/Images/FoodOrgImages/org_img${data[i]["id"]}.png`;
        data[i].link = `charity_organizations/${data[i]["id"]}`;
      }
    } else if (modelType.toLowerCase().startsWith("grocery")) {
      response = await axios.get(
        "https://api.texasfoodinsecurity.me/grocerystores?page_size=1000"
      );
      data = response.data["grocerystores"];

      let county_response = await axios.get(
        "https://api.texasfoodinsecurity.me/county?page_size=1000"
      );
      let county_data = county_response.data["county"];
      for (const card of data) {
        card["county"] = county_data[card["county_id"] - 1]
          ? county_data[card["county_id"] - 1]["name"]
          : "";
      }
      for (let i = 0; i < data.length; i++) {
        data[i][
          "Image"
        ] = `/Images/GroceryStoreImages/groc_img${data[i]["id"]}.png`;
        data[i].link = `grocery_stores/${data[i]["id"]}`;
      }
    } else if (modelType.toLowerCase().startsWith("count")) {
      response = await axios.get(
        "https://api.texasfoodinsecurity.me/county?page_size=1000"
      );
      data = response.data["county"];
      for (let i = 0; i < data.length; i++) {
        data[i][
          "Image"
        ] = `/Images/CountyImages/county_img${data[i]["id"]}.png`;
        data[i].link = `counties/${data[i]["id"]}`;
      }
    } else if (modelType.toLowerCase() === "foodorg_grocerystore") {
      response = await axios.get(
        "https://api.texasfoodinsecurity.me/foodorg_grocerystore/all"
      );
      data = response.data["foodorg_grocerystore"];
    } else if (modelType.toLowerCase() === "foodorg_county") {
      response = await axios.get(
        "https://api.texasfoodinsecurity.me/foodorg_county/all"
      );
      data = response.data["foodorg_counties"];
    } else if (modelType.toLowerCase().startsWith("full_foodorg")) {
      response = await axios.get(
        "https://api.texasfoodinsecurity.me/foodorg?page_size=1000"
      );
      data = response.data["foodorg"];
      for (let i = 0; i < data.length; i++) {
        data[i]["Image"] = `/Images/FoodOrgImages/org_img${data[i]["id"]}.png`;
        data[i].link = `charity_organizations/${data[i]["id"]}`;
      }
    }
    return data
}

export function useDebounce(value, delay) {
    const [debouncedValue, setDebouncedValue] = useState(value);
  
    useEffect(() => {
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);
  
      return () => {
        clearTimeout(handler);
      };
    }, [value, delay]);
  
    return debouncedValue;
  }

export const getInstanceData = async (id, modelType) => {
    let response = {};
    let data = [];
    if (modelType.toLowerCase().startsWith('charity')) {
        response = await axios.get(`https://api.texasfoodinsecurity.me/foodorg/${id}`);
        data = response.data['foodorg']
        data["Image"] = `/Images/FoodOrgImages/org_img${id}.png`;
        data.link = `charity_organizations/${id}`
    } else if(modelType.toLowerCase().startsWith('grocery')) {
        response = await axios.get(`https://api.texasfoodinsecurity.me/grocerystores/${id}`);
        data = response.data['grocerystores']
        data["Image"] = `/Images/GroceryStoreImages/groc_img${id}.png`;
        data.link = `grocery_stores/${id}`
  
        let county_response = await axios.get(`https://api.texasfoodinsecurity.me/county/${data.county_id}`);
        let county_data = county_response.data['county']
        data['county'] = county_data.name;
    } else if(modelType.toLowerCase().startsWith('counties')) {
        response = await axios.get(`https://api.texasfoodinsecurity.me/county/${id}`);
        data = response.data['county']
        data['Image'] = `/Images/CountyImages/county_img${id}.png`;
        data.link = `counties/${id}`
    }
    return data
}

export const getSearchData = async (modelType, query) => {
    let response = {};
    let data = [];
    if (modelType.toLowerCase().startsWith('charity')) {
        response = await axios.get(`https://api.texasfoodinsecurity.me/search/foodorg?terms=${query}`);
        data = response.data['data']
        
        // remove duplicates
        const seenNames = new Set();
        let filteredData = [];
        for (let i = data.length-1; i >= 0; i--) {
            if (seenNames.has(data[i].name)) {
                
            } else {
                seenNames.add(data[i].name)
                filteredData.push(data[i])
            }
        }
        data = filteredData.reverse()
        for (let i = 0; i < data.length; i++) {
            data[i][
              "Image"
            ] = `/Images/FoodOrgImages/org_img${data[i]["id"]}.png`;
            data[i].link = `charity_organizations/${data[i]['id']}`;
        }
    } else if(modelType.toLowerCase().startsWith('grocery')) {
        response = await axios.get(`https://api.texasfoodinsecurity.me/search/grocerystore?terms=${query}`);
        data = response.data['data']

        let county_response = await axios.get("https://api.texasfoodinsecurity.me/county?page_size=1000");
        let county_data = county_response.data['county']
        for (const card of data) {
            card['county'] = county_data[card['county_id']-1] ? county_data[card['county_id']-1]['name'] : '';
        }
        for (let i = 0; i < data.length; i++) {
            data[i][
              "Image"
            ] = `/Images/GroceryStoreImages/groc_img${data[i]["id"]}.png`;
            data[i].link = `grocery_stores/${data[i]['id']}`;
        }
    } else if(modelType.toLowerCase().startsWith('counties')) {
        response = await axios.get(`https://api.texasfoodinsecurity.me/search/county?terms=${query}`);
        data = response.data['data']
        for (let i = 0; i < data.length; i++) {
            data[i]['Image'] = `/Images/CountyImages/county_img${data[i]['id']}.png`;
            data[i].link = `counties/${data[i]['id']}`;
        }
    } 
    for (let i = 0; i < data.length; i++) { 
        for (let key of Object.keys(data[i])) {
            if (key.toLowerCase() !== "image" && !key.includes("id") && key !== 'link') {
                if (typeof data[i][key] === 'string') {
                    let lowercase_regex = new RegExp(query, 'g');
                    let uppercase_regex = new RegExp(toTitleCase(query), 'g')
                    data[i][key] = toTitleCase(data[i][key])
                    data[i][key] = data[i][key].replace(lowercase_regex, `<mark>${query}</mark>`)
                    data[i][key] = data[i][key].replace(uppercase_regex, `<mark>${toTitleCase(query)}</mark>`)
                }
            }
        }
    }
    return data
}

export const handleHighlight = (input) => {
    const markup = {__html: input};
    return <div dangerouslySetInnerHTML={markup}></div>
}

export const getSortedAndFilteredData = async (modelType, sortkey, order, filter, filterCond, filterNum) => {
    let response = {};
    let data = [];
    if (!modelType) {
        return data;
    }
    if (modelType.toLowerCase().startsWith("charity")) {
        response = await axios.get(
          `https://api.texasfoodinsecurity.me/foodorg?sort_by=${sortkey}&order=${order}&page_size=1000&filter=${filter}`
        );
        data = response.data['foodorg']

        // remove duplicates
        const seenNames = new Set();
        let filteredData = [];
        for (let i = data.length-1; i >= 0; i--) {
            if (seenNames.has(data[i].name)) {
                
            } else {
                seenNames.add(data[i].name)
                filteredData.push(data[i])
            }
        }
        data = filteredData.reverse()
        for (let i = 0; i < data.length; i++) {
            data[i]['Image'] = `/Images/FoodOrgImages/org_img${data[i]['id']}.png`;
            data[i].link = `charity_organizations/${data[i]['id']}`;
        }
    } else if(modelType.toLowerCase().startsWith("grocery")) {
        response = await axios.get(`https://api.texasfoodinsecurity.me/grocerystores?sort_by=${sortkey}&order=${order}&page_size=1000&filter=${filter}`);
        data = response.data['grocerystores']

        let county_response = await axios.get(`https://api.texasfoodinsecurity.me/county?page_size=1000`);
        let county_data = county_response.data['county']
        for (const card of data) {
            card['county'] = county_data[card['county_id']-1] ? county_data[card['county_id']-1]['name'] : '';
        }
        for (let i = 0; i < data.length; i++) {
            data[i][
              "Image"
            ] = `/Images/GroceryStoreImages/groc_img${data[i]["id"]}.png`;
            data[i].link = `grocery_stores/${data[i]['id']}`;
        }
    } else if(modelType.toLowerCase().startsWith("count")) {
        response = await axios.get(`https://api.texasfoodinsecurity.me/county?` +
        `sort_by=${sortkey}&order=${order}&page_size=1000&` +
        `filter=${filter}&filterCond=${filterCond}&filterNum=${filterNum}`);
        data = response.data['county']
        for (let i = 0; i < data.length; i++) {
            data[i]['Image'] = `/Images/CountyImages/county_img${data[i]['id']}.png`;
            data[i].link = `counties/${data[i]['id']}`;
        }
    }
    return data
}