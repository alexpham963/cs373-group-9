import React from "react";
import CardContainer from "../components/CardContainer";
import "../index.css";
import { useState, useEffect } from "react";
import { filterDisplayableAttr, getModelData, getSearchData, getSortedAndFilteredData, toTitleCase, useDebounce } from "../utils/utils";
import { Dropdown } from "react-bootstrap";
import "../styles/ModelPage.css"


const ModelPage = ({modelType}) => {
    
    const [cardDataList, setCardDataList] = useState([]);
    //handle sort and filter
    const [sortBy, setSortBy] = useState('Sort By');
    const [sortOrder, setSortOrder] = useState('asc');
    const [filterBy, setFilterBy] = useState('Filter By');
    const [filterCond, setFilterCond] = useState('Greater Than');
    const [filterNum, setFilterNum] = useState(0)
    const filterByList = [
      "num_children_insecure",
      "num_people_insecure",
      "percent_children_insecure",
      "percent_people_insecure",
      "Nothing",
    ];
    const grocFilterList = ['snap', "Nothing"]
    const charFilterList = ["bank", "pantry", "food_kitchen", "Nothing"];
    //handle search
    const [query, setQuery] = useState('');
    const debouncedQuery = useDebounce(query, 300);
    
    //get data on load in
    useEffect(() => {
        setSortBy('Sort By')
        setSortOrder('asc')
        setQuery('')
        setFilterBy('Filter By')
        setFilterCond('Greater Than')
        setFilterNum(0)
        getModelData(modelType, setCardDataList).then(modelData => {
            setCardDataList(modelData)
      });
    } ,[modelType])

    //handle search
    const handleQueryChange = (event) => {
        const newQuery = event.target.value;
        setQuery(newQuery);
    };
    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            handleQueryChange(event);
        }
    }
    useEffect(() => {
        setSortBy('Sort By')
        setSortOrder('asc') 
        setFilterBy('Filter By')
        setFilterCond('Greater Than')
        setFilterNum(0)
        if (debouncedQuery) {
            getSearchData(modelType, debouncedQuery).then(modelData => {
                setCardDataList(modelData)
            });
        } else {
            getModelData(modelType, setCardDataList).then(modelData => {
                setCardDataList(modelData)
            });
        }
    }, [debouncedQuery, modelType])

    //handle sort and filter
    const handleFilterNumChange = (event) => {
        const newNum = event.target.value;
        if (!isNaN(newNum)) {
            setFilterNum(newNum);
        }
    };
    useEffect(() => {
        let sort = sortBy === 'Sort By' ? 'name' : sortBy;
        getSortedAndFilteredData(modelType, sort, sortOrder, filterBy, filterCond, +filterNum).then(modelData => {
            setCardDataList(modelData)
        })
    //eslint-disable-next-line
    }, [sortBy, sortOrder, filterBy, filterCond, filterNum])

    return (
      <div className={`page-background`}>
        <div
          className="description-container"
          style={{ justifyContent: "center", alignContent: "center" }}
        >
          <div className="description-section">
            <h1>{modelType}</h1>
            {modelType === "Charity Organizations" ? (
              <strong>
                Welcome to our Charity Organizations page. Here, you'll find a
                diverse collection of organizations in Texas dedicated to
                combating hunger and providing essential services. This includes
                food banks, food kitchens, and soup kitchens. You can explore
                each organization's, where they are located, and counties they
                serve, and what grocery stores are connected to those counties
                when clicking on a card. Whether you're looking to donate,
                volunteer, or simply learn more about these incredible
                organizations, we hope this resource serves as a valuable tool
                in your journey.
              </strong>
            ) : null}
            {modelType === "Grocery Stores" ? (
              <strong>
                Welcome to our Grocery Stores page. Here, you'll find a diverse
                collection of stores in Texas dedicated to providing a wide
                range of food products and essential goods. This includes local
                markets, large supermarkets, and specialty food stores. You can
                explore each store's location, and the counties they are in when
                clicking on a card. Whether you're planning your next shopping
                trip or exploring stores that are SNAP elgible we hope this
                resource serves as a valuable tool in your journey.
              </strong>
            ) : null}
            {modelType === "Counties" ? (
              <strong>
                Welcome to our Counties page. Here, you'll find a comprehensive
                list of all counties in Texas. You can explore each county's
                food insecurity statistics, and the charity organizations and
                grocery stores serving that county when clicking on a card.
                Whether you're researching for academic purposes, planning a
                move, or simply interested in learning more about the diverse
                regions of Texas, we hope this resource serves as a valuable
                tool in your journey.
              </strong>
            ) : null}
            <p style={{ fontWeight: "bold" }}>
              Instances: {cardDataList.length}
            </p>
          </div>
        </div>
        <div className="filter-div">
          <div className="dropdown-div">
            <Dropdown>
              <Dropdown.Toggle
                id="dropdown-basic"
                style={{
                  backgroundColor: "var(--primary-alt-color)",
                  color: "var(--primary-color)",
                }}
              >
                {toTitleCase(sortBy.toString().replaceAll("_", " "))}
              </Dropdown.Toggle>
              <Dropdown.Menu
                style={{
                  backgroundColor: "var(--primary-alt-color)",
                  color: "var(--primary-color)",
                }}
              >
                {cardDataList[0]
                  ? Object.keys(
                      filterDisplayableAttr(cardDataList[0], modelType)
                    ).map((key) => {
                      return (
                        <Dropdown.Item
                          key={key}
                          style={{
                            backgroundColor: "var(--primary-alt-color)",
                            color: "var(--primary-color)",
                          }}
                          onClick={() => setSortBy(key)}
                        >
                          {toTitleCase(key.toString().replaceAll("_", " "))}
                        </Dropdown.Item>
                      );
                    })
                  : ""}
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown>
              <Dropdown.Toggle
                id="dropdown-basic"
                style={{
                  backgroundColor: "var(--primary-alt-color)",
                  color: "var(--primary-color)",
                }}
              >
                {toTitleCase(sortOrder)}
              </Dropdown.Toggle>
              <Dropdown.Menu
                style={{
                  backgroundColor: "var(--primary-alt-color)",
                  color: "var(--primary-color)",
                }}
              >
                <Dropdown.Item
                  key={1}
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                  onClick={() => setSortOrder("asc")}
                >
                  Ascending
                </Dropdown.Item>
                <Dropdown.Item
                  key={2}
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                  onClick={() => setSortOrder("desc")}
                >
                  Descending
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
          {modelType !== "Charity Organizations" ? (
            ""
          ) : (
            <div className="dropdown-div">
              <Dropdown>
                <Dropdown.Toggle
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                  id="dropdown-basic"
                >
                  {toTitleCase(filterBy.toString().replaceAll("_", " "))}
                </Dropdown.Toggle>
                <Dropdown.Menu
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                >
                  {charFilterList.map((key) => {
                    return (
                      <Dropdown.Item
                        key={key}
                        style={{
                          backgroundColor: "var(--primary-alt-color)",
                          color: "var(--primary-color)",
                        }}
                        onClick={() => setFilterBy(key)}
                      >
                        {toTitleCase(key.toString().replaceAll("_", " "))}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </div>
          )}
          {modelType !== "Grocery Stores" ? (
            ""
          ) : (
            <div className="dropdown-div">
              <Dropdown>
                <Dropdown.Toggle
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                  id="dropdown-basic"
                >
                  {toTitleCase(filterBy.toString().replaceAll("_", " "))}
                </Dropdown.Toggle>
                <Dropdown.Menu
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                >
                  {grocFilterList.map((key) => {
                    return (
                      <Dropdown.Item
                        key={key}
                        style={{
                          backgroundColor: "var(--primary-alt-color)",
                          color: "var(--primary-color)",
                        }}
                        onClick={() => setFilterBy(key)}
                      >
                        {toTitleCase(key.toString().replaceAll("_", " "))}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </div>
          )}
          {modelType !== "Counties" ? (
            ""
          ) : (
            <div className="dropdown-div">
              <Dropdown>
                <Dropdown.Toggle
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                  id="dropdown-basic"
                >
                  {toTitleCase(filterBy.toString().replaceAll("_", " "))}
                </Dropdown.Toggle>
                <Dropdown.Menu
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                >
                  {filterByList.map((key) => {
                    return (
                      <Dropdown.Item
                        key={key}
                        style={{
                          backgroundColor: "var(--primary-alt-color)",
                          color: "var(--primary-color)",
                        }}
                        onClick={() => setFilterBy(key)}
                      >
                        {toTitleCase(key.toString().replaceAll("_", " "))}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
              <Dropdown>
                <Dropdown.Toggle
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                  id="dropdown-basic"
                >
                  {toTitleCase(filterCond)}
                </Dropdown.Toggle>
                <Dropdown.Menu
                  style={{
                    backgroundColor: "var(--primary-alt-color)",
                    color: "var(--primary-color)",
                  }}
                >
                  <Dropdown.Item
                    key={1}
                    style={{
                      backgroundColor: "var(--primary-alt-color)",
                      color: "var(--primary-color)",
                    }}
                    onClick={() => setFilterCond("greaterThan")}
                  >
                    Greater Than
                  </Dropdown.Item>
                  <Dropdown.Item
                    key={2}
                    style={{
                      backgroundColor: "var(--primary-alt-color)",
                      color: "var(--primary-color)",
                    }}
                    onClick={() => setFilterCond("lessThan")}
                  >
                    Less Than
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
              <form>
                <input
                  type="text"
                  placeholder="Filter Num"
                  value={filterNum}
                  onChange={handleFilterNumChange}
                  style={{ width: "80px" }}
                ></input>
              </form>
            </div>
          )}
          <div className="searchBar">
            <form>
              <input
                type="text"
                placeholder="Enter Search"
                value={query}
                onChange={handleQueryChange}
                onKeyDown={handleKeyPress}
              ></input>
            </form>
          </div>
        </div>
        <CardContainer data={cardDataList}></CardContainer>
      </div>
    );
};
export default ModelPage;
