# get all counties first
all_counties = []
counties = open("backend/Counties/CountyInfo.txt", "r")

counter = 0
for line in counties:
    if counter > 0:
        all_counties.append(line.split(",")[0].strip())
    counter += 1

print(all_counties)

# get all counties in GroceryStore1.txt
counties_1 = {}
stores_1 = open("backend/GroceryStores/GroceryStore.txt")

counter = 0
for line in stores_1:
    if counter > 0:
        county = line.split(",")[1].strip()
        if county not in counties_1:
            counties_1[county] = [line]
        else:
            counties_1[county].append(line)
    counter += 1

# get all frames for GroceryStore1.txt
frames_1 = {}
maps_1 = open("frontend/public/Maps/store_maps.txt")

counter = 0
for line in maps_1:
    if counter > 0:
        county = line.split(",")[1].strip()
        if county not in counties_1:
            counties_1[county] = [line]
        else:
            counties_1[county].append(line)
    counter += 1

# get all counties in GroceryStore2.txt
counties_2 = {}
stores_2 = open("backend/GroceryStores/GroceryStore2.txt")

counter = 0
for line in stores_2:
    if counter > 0:
        county = line.split(",")[1].strip()
        if county not in counties_2:
            counties_2[county] = [line]
        else:
            counties_2[county].append(line)
    counter += 1

# check location of all counties
stores_3 = open("backend/GroceryStores/GroceryStore3.txt", "w")
for county in all_counties:

    if county in counties_2:
        for line in counties_2[county]:
            stores_3.write(line)

    elif county in counties_1:
        for line in counties_1[county]:
            stores_3.write(line)
