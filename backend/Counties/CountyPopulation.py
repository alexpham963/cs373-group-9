from bs4 import BeautifulSoup
import requests

code_url = "https://services2.arcgis.com/5MVN2jsqIrNZD4tP/arcgis/rest/services/Counties/FeatureServer/0/query?where=FENAME%20%3D%20%27COUNTY%27&outFields=FENAME,CNTYFIPS&returnGeometry=false&outSR=4326&f=json"

pop_url = "https://worldpopulationreview.com/us-counties/tx/"

counties = open("CountyInfo.txt", "r")
for line in counties:

    # Get county number
    county = line.split(",")[0]
    new_url = code_url.replace("COUNTY", county.upper().replace("-", " "))
    r = requests.get(new_url)
    data = r.json()

    if (len(data["features"])) == 0:
        print(county)
    else:
        attributes = data["features"][0]["attributes"]
        county_code = int((int(attributes["CNTYFIPS"][2:]) + 1) / 2)

        # Get population
        county_input = county.replace(" ", "-")
        r = requests.get(pop_url + county_input + "-county-population")
        soup = BeautifulSoup(r.content, "html5lib")
        info = str(soup.find_all("b")[0])
        population = info[3 : len(info) - 4].replace(",", "")

        print(county + "," + str(county_code) + "," + population)
