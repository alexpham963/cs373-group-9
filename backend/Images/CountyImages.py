from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import time

# Set up and get web page
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

url = "https://en.wikipedia.org/wiki/List_of_counties_in_Texas"
driver.get(url)
time.sleep(1)

links = []
for i in range(1, 255):
    xpath = (
        "/html/body/div[2]/div/div[3]/main/div[3]/div[3]/div[1]/table[2]/tbody/tr["
        + str(i)
        + "]/td[8]/span/a/img"
    )

    # scrape map image of each county
    photo = driver.find_element(By.XPATH, xpath)
    link = photo.get_attribute("src")
    print(link)
    links.append(link)

# write links to text file
file = open("backend/Images/CountyImages.txt", "w")
for link in links:
    file.write(link)
    file.write("\n")
