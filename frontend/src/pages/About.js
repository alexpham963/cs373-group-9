import React, { useState, useEffect } from "react";
import axios from "axios";
import "../index.css";
import "../styles/About.css"

// Import images

import Alex from "../images/about_images/Alex.jpg";
import Eric from "../images/about_images/Eric.jpg";
import Pascal from "../images/about_images/Pascal.jpg";
import Maxwell from "../images/about_images/Maxwell.jpg";
import Aditya from "../images/about_images/Aditya.jpg";
import aws from "../images/about_images/aws.png";
import bootstrap from "../images/about_images/bootstrap.png";
import docker from "../images/about_images/docker.png";
import gitlab from "../images/about_images/gitlab.png";
import google from "../images/about_images/google.png";
import postman from "../images/about_images/postman.png";
import python from "../images/about_images/python.png";
import react from "../images/about_images/react.png";
import slack from "../images/about_images/slack.png";
import vscode from "../images/about_images/vscode.png";
import flask from "../images/about_images/flask.png";
import selenium from "../images/about_images/selenium.png";

const initialUserData = [
  {
    img: Alex,
    name: "Alex Pham",
    bio: "I am a CS major in my senior year, graduating by the end of 2024. I'm from Garland, TX, I play the trumpet and currently playing Elden Ring.",
    gitLab: ["alexpham963", "Alex P Pham"],
    Rez: "Fullstack",
    commits: 0,
    issues: 0,
    unittest: 15,
  },
  {
    img: Eric,
    name: "Eric Rodriguez",
    bio: "I am a Junior majoring in computer science with a minor in business. Born and raise in the Austin area in Texas. One recommendation I have is to play Ghost of Tsushima, it will not disappoint.",
    gitLab: ["Eric", "Keeylo", "Eric Rodriguez"],
    Rez: "Frontend",
    commits: 0,
    issues: 0,
    unittest: 18,
  },
  {
    img: Pascal,
    name: "Pascal Garcia",
    bio: "I am a Sophomore computer science major. I was born and raised in Austin, TX, and I enjoy playing tennis and taking long walks.",
    gitLab: ["garcia_pascal", "Pascal"],
    Rez: "Backend",
    commits: 0,
    issues: 0,
    unittest: 16,
  },
  {
    img: Maxwell,
    name: "Maxwell Wu",
    bio: "I am a Junior in the CSB program. I grew up in Dallas, and some of my hobbies include the string bass, tennis, and video games.",
    gitLab: ["Maxwell Wu", "25wum", "Maxwell M Wu"],
    Rez: "Frontend",
    commits: 0,
    issues: 0,
    unittest: 15,
  },
  {
    img: Aditya,
    name: "Aditya Jagarlamudi",
    bio: "I am a CS major currently in my sophomore year. I'm from Longview, TX, and my interests include tennis, video games, and mental math tricks.",
    gitLab: ["adityajagarlamudi", "Aditya Jagarlamudi", "adityaj12"],
    Rez: "Backend",
    commits: 0,
    issues: 0,
    unittest: 16,
  },
];

const fetchGitLabData = async (projectId, setUserData) => {
  try {
    // Fetch all issues for the project
    const issuesResponse = await axios.get(
      `https://gitlab.com/api/v4/projects/${projectId}/issues?per_page=100&state=all`
    );

    // Fetch all commits for the project
    const allCommits = await fetchAllCommits(projectId);

    const updatedUsers = await Promise.all(
      initialUserData.map(async (user) => {
        if (!user.gitLab || user.gitLab.length === 0) return user;

        let commitsCount = 0;
        let issuesCount = 0;

        user.gitLab.forEach((gitLabId) => {
          commitsCount += allCommits.filter(
            (commit) =>
              commit.author_name.toLowerCase() === gitLabId.toLowerCase()
          ).length;
          issuesCount += issuesResponse.data.filter(
            (issue) =>
              issue.closed_by &&
              issue.closed_by.username.toLowerCase() === gitLabId.toLowerCase()
          ).length;
        });

        return { ...user, commits: commitsCount, issues: issuesCount };
      })
    );

    setUserData(updatedUsers);
  } catch (error) {
    console.error("Error fetching GitLab data:", error);
  }
};

const fetchAllCommits = async (projectId) => {
  let allCommits = [];
  let page = 1;

  // Fetch all commits in the project, not just per branch.
  while (true) {
    const response = await axios.get(
      `https://gitlab.com/api/v4/projects/${projectId}/repository/commits`,
      { params: { per_page: 100, page } }
    );

    if (response.data.length === 0) {
      break;
    }

    allCommits = [...allCommits, ...response.data];
    page += 1;
  }

  return allCommits;
};


const About = () => {
  const [userData, setUserData] = useState([]);
  const projectId = "54620473";
  // Get Git Data
  useEffect(() => {
    fetchGitLabData(projectId, setUserData);
  }, [projectId]);

  // Total of all users commits and issues
  const totalCommits = userData.reduce(
    (total, user) => total + user.commits,
    0
  );
  const totalIssues = userData.reduce((total, user) => total + user.issues, 0);

  return (
    <div className={`page-background`}>
      <div className="controls"></div>
      <div className="description-container">
        <div className="description-section">
          <h1>Fighting Food Insecurity in Texas</h1>
          <strong style={{ padding: "1%" }}>
            We are a passionate team of five computer science students united by
            a shared vision: to combat food insecurity in Texas through the
            power of technology. Our journey began in a university classroom,
            where we were inspired by the potential of digital solutions to
            address some of society's most pressing issues. Food insecurity, a
            critical challenge affecting millions of Texans, stood out to us as
            an area where we could make a significant impact.
          </strong>
          <div style={{ padding: "1%" }}></div>
          <h1>Purpose</h1>
          <strong>
            The TXFoodInsecurity project, developed by Group 9, aims to combat
            food insecurity in Texas by facilitating access to food assistance
            services and raising awareness about the issue. Utilizing data from
            various sources, including food banks, research institutions, and
            government databases, the team has created a digital platform that
            serves as a comprehensive resource for individuals in need. Our goal
            for this project is to build a website that will connect people
            affected by food insecurity to places and sites that will help aid
            them in their struggle. Our website will make it easy to search for
            food organizations such as food banks, soup kitchens, and food
            pantries that are able to grant and supplement meals to their needy
            beneficiaries. In addition, our website will display nearby grocery
            stores so that our users can find out their closest places to
            purchase food. Lastly, our website provides statistics on food
            insecurity in Texas counties in order to spread awareness and also
            displays nearby food organizations.
          </strong>
          <div style={{ padding: "5%" }}></div>
        </div>
        <div className="description-section">
          <h1>The Team</h1>
        </div>
        <div className="id-card-container">
          {/* Create a row of id-cards and display info */}
          {userData.map((user, index) => (
            <div className="id-card" key={index}>
              <img src={user.img} alt={`contributor ${index + 1}`} />
              <h3>{user.name}</h3>
              <p>Bio: {user.bio}</p>
              <p>Responsibilities: {user.Rez}</p>
              <p>Commits: {user.commits}</p>
              <p>Issues: {user.issues}</p>
              <p>Unit Tests: {user.unittest}</p>
            </div>
          ))}
        </div>
        {/* Total Commits + Issues */}
        <div className="description-section">
          <h1>Total Commits: {totalCommits}</h1>
          <h1>Total Issues: {totalIssues}</h1>
          <h1>Total Unit Tests: 80</h1>
        </div>
        <div className="description-section">
          <h1>Project Links</h1>
          <div className="id-card-container">
            <div
              className="center-text"
              style={{ width: "70%", padding: "5%" }}
            >
              <li>
                <a href="https://gitlab.com/alexpham963/cs373-group-9">
                  GitLab Repo
                </a>
              </li>
              <li>
                <a href="https://documenter.getpostman.com/view/32886907/2sA2r535HM">
                  Postman API Documentation
                </a>
              </li>
            </div>
          </div>
        </div>
        <div className="description-section">
          <div className="space">
            <h1>Data</h1>
            <div
              className="center-text"
              style={{ width: "70%", marginLeft: "15%" }}
            >
              <div style={{ padding: "5%" }}>
                <li>
                  <a href="https://www.homelessshelterdirectory.org/foodbanks/state/texas">
                    Soup Kitchens
                  </a>
                </li>
                <li>
                  <a href="https://www.centraltexasfoodbank.org/">
                    Central Texas Food Bank
                  </a>
                </li>
                <li>
                  <a href="https://hungerandpoverty.web.baylor.edu/our-approach/research-and-education/hunger-data-lab/data">
                    Baylor University Hunger and Poverty
                  </a>
                </li>
                <li>
                  <a href="https://squaremeals.org/Public-Resources/Texas-Food-Banks">
                    Texas Food Banks - Square Meals
                  </a>
                </li>
                <li>
                  <a href="https://map.feedingamerica.org/county/2021/overall/texas">
                    Feeding America - Texas County Data
                  </a>
                </li>
                <li>
                  <a href="https://fdc.nal.usda.gov/">
                    USDA FoodData Central (RESTful API)
                  </a>
                </li>
                <li>
                  <a href="https://datausa.io/about/api/">Data USA</a>
                </li>
                <li>
                  <a href="https://www.hhs.texas.gov/sites/default/files/documents/doing-business-with-hhs/provider-portal/wic/vendors/vendor-a-d.pdf">
                    HHS
                  </a>
                </li>
                <li>
                  <a href="https://worldpopulationreview.com/zips/texas">
                    World Population View
                  </a>
                </li>
                <li>
                  <a href="https://schoolsdata2-tea-texas.opendata.arcgis.com/datasets/c71146b6426248a5a484d8b3c192b9fe_0/api">
                    Texas Education Agency (RESTful API)
                  </a>
                </li>
              </div>
            </div>
          </div>
        </div>
        {/* Tools */}
        <div className="description-section">
          <h1>Tools</h1>
        </div>
        <div className="id-card-container">
          <div className="id-card">
            <img src={aws} alt="AWS" />
            <h3>AWS</h3>
          </div>
          <div className="id-card">
            <img src={bootstrap} alt="Bootstrap" />
            <h3>Bootstrap</h3>
          </div>
          <div className="id-card">
            <img src={docker} alt="Docker" />
            <h3>Docker</h3>
          </div>
          <div className="id-card">
            <img src={flask} alt="Flask" />
            <h3>Flask</h3>
          </div>
          <div className="id-card">
            <img src={gitlab} alt="GitLab" />
            <h3>GitLab</h3>
          </div>
          <div className="id-card">
            <img src={google} alt="Google" />
            <h3>Google</h3>
          </div>
          <div className="id-card">
            <img src={postman} alt="Postman" />
            <h3>Postman</h3>
          </div>
          <div className="id-card">
            <img src={python} alt="Python" />
            <h3>Python</h3>
          </div>
          <div className="id-card">
            <img src={react} alt="React" />
            <h3>React</h3>
          </div>
          <div className="id-card">
            <img src={selenium} alt="Selenium" />
            <h3>Selenium</h3>
          </div>
          <div className="id-card">
            <img src={slack} alt="Slack" />
            <h3>Slack</h3>
          </div>
          <div className="id-card">
            <img src={vscode} alt="VSCode" />
            <h3>VSCode</h3>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
