import React, { useState } from "react";
import "../styles/Navbar.css";
import {
  Nav,
  Navbar,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

const CustomNavbar = () => {
  const [query, setQuery] = useState("");

  const tooltip = (props) => {
    return <Tooltip {...props} id="tooltip"></Tooltip>;
  };

  const toggleDarkMode = () => {
    const current = document.documentElement.getAttribute("data-theme");
    const newTheme = current === "light" ? "dark" : "light";
    document.documentElement.setAttribute("data-theme", newTheme);
  };

  const detectLanguage = () => {
    return window.location.href.includes("tl=es") ? "es" : "en";
  };

  const toggleLanguage = () => {
    const language = detectLanguage();
    if (language === "en") {
      window.location.href =
        "https://www-texasfoodinsecurity-me.translate.goog/?_x_tr_sl=auto&_x_tr_tl=es&_x_tr_hl=en&_x_tr_pto=wapp";
    } else {
      window.location.href = "https://www.texasfoodinsecurity.me";
    }
  };
  
  const navigate = useNavigate();
  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      navigate(`/search?query=${query}`)
      // handleQueryChange(event);
    }
  }
  const handleSearchChange = (event) => {
    setQuery(event.target.value);
  }

  return (
    <Navbar expand="xl" className="navbar">
        <Navbar.Brand as={Link} to="/">
          <h1 className="website-name" style={{ textDecoration: "none"}}>
            Texas Food Insecurity
          </h1>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <div className="links">
            <Nav>
              <Link
                to="/"
                className="nav-link"
                style={{ color: "var(--primary-color)" }}
              >
                Home
              </Link>
              <Link
                to="/about"
                className="nav-link"
                style={{ color: "var(--primary-color)" }}
              >
                About
              </Link>
              <Link
                to="/charity_organizations"
                className="nav-link"
                style={{ color: "var(--primary-color)" }}
              >
                Charity Organizations
              </Link>
              <Link
                to="/grocery_stores"
                className="nav-link"
                style={{ color: "var(--primary-color)" }}
              >
                Grocery Stores
              </Link>
              <Link
                to="/counties"
                className="nav-link"
                style={{ color: "var(--primary-color)" }}
              >
                Counties
              </Link>
              <Link
                to="/visualizations"
                className="nav-link"
                style={{ color: "var(--primary-color)" }}
              >
                Visuals
              </Link>
              <Link
                to="/provider_visuals"
                className="nav-link"
                style={{ color: "var(--primary-color)" }}
              >
                Provider Visuals
              </Link>
              <div className="nav-link">
                <form>
                  <input 
                    type="text" 
                    placeholder="Enter Search" 
                    style={{width: '150px'}}
                    value={query} 
                    onChange={handleSearchChange} 
                    onKeyDown={handleKeyPress}>
                  </input>
                </form>
              </div>
            </Nav>
            <button
              onClick={toggleLanguage}
              style={{
                background: "none",
                border: "none",
                color: "inherit", // Use the current text color
                textDecoration: "underline", // Make it look like a link
                cursor: "pointer",
                font: "inherit",
              }}
              aria-label="Toggle Language"
            >
              {detectLanguage() === "en" ? "Español" : "English"}
            </button>
            <OverlayTrigger placement="bottom" overlay={tooltip}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  padding: "0 0 2px 0",
                }}
              >
                {/* this goes over window size limit */}
                <span>🌞</span> {/* Text for Light Mode */}
                <label className="switch">
                  <input
                    type="checkbox"
                    id="theme-toggle"
                    onChange={toggleDarkMode}
                  />
                  <span className="slider round"></span>
                </label>
                <span >🌚</span> {/* Text for Dark Mode */}
              </div>
            </OverlayTrigger>
          </div>
        </Navbar.Collapse>
    </Navbar>
  );
};

export default CustomNavbar;