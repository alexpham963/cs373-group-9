from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import time

# Set up and get web page
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

url = "https://www.google.com/imghp"
time.sleep(1)

counter = 0
file = open("backend/FoodOrgs/FoodOrg3.txt")
for line in file:

    if "name of food org" not in line:
        driver.get(url)
        time.sleep(1)

        # turn line into search term
        info = line.split(",")
        search = info[0] + " " + info[1] + " " + info[2] + " " + info[3]
        print(info[0])

        try:
            # screenshot first image result on google search
            search_box = driver.find_element(
                By.XPATH,
                "/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/textarea",
            )
            search_box.send_keys(search)
            search_box.send_keys(Keys.ENTER)

            time.sleep(1)
            img = driver.find_element(
                By.XPATH,
                "/html/body/div[2]/c-wiz/div[3]/div[1]/div/div/div/div/div[1]/div[1]/span/div[1]/div[1]/div[1]/a[1]/div[1]/img",
            )
            img.screenshot("backend/Images/FoodOrgImages")
            img.screenshot(
                "backend/Images/FoodOrgImages/food_org_img" + str(counter) + ".png"
            )
        except NoSuchElementException:
            print(line)

    counter += 1
