import React, { useRef, useEffect } from 'react';
import * as d3 from 'd3';

const ScatterPlot = ({ data, width = 500, height = 400 , title, xaxis_label="", yaxis_label=""}) => {
  const d3Container = useRef(null);

  useEffect(() => {
    if (data && d3Container.current) {
      const svg = d3.select(d3Container.current);

      // Clear SVG to prevent duplication
      svg.selectAll("*").remove();

      // Set up dimensions and margins
      const margin = { top: 40, right: 30, bottom: 60, left: 70 },
            plotWidth = width - margin.left - margin.right,
            plotHeight = height - margin.top - margin.bottom;

      // Append the svg object to the div
      const svgContent = svg
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", `translate(${margin.left},${margin.top})`);

      // Set up the scales
      const x = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.x)])
        .range([0, plotWidth]);
      svgContent.append("g")
        .attr("transform", `translate(0,${plotHeight})`)
        .call(d3.axisBottom(x));

      const y = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.y)])
        .range([plotHeight, 0]);
      svgContent.append("g")
        .call(d3.axisLeft(y));

      // Add X axis label
      svgContent.append("text")
        .style("fill", "var(--text-color)")
        .attr("text-anchor", "middle")
        .attr("x", plotWidth / 2)
        .attr("y", plotHeight + margin.top)
        .text(xaxis_label);

      // Add Y axis label
      svgContent.append("text")
        .style("fill", "var(--text-color)")
        .attr("text-anchor", "middle")
        .attr("transform", "rotate(-90)")
        .attr("y", -margin.left + 20)
        .attr("x", (-margin.top - plotHeight) / 2)
        .text(yaxis_label);

      // Add Title
      svgContent.append("text")
        .attr("x", (plotWidth / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "16px") 
        .style("text-decoration", "underline")  
        .style("fill", "var(--text-color)")
        .text(title);

      // Tooltip setup
      const tooltipDiv = d3.select(d3Container.current.parentNode)
        .append("div")
        .style("position", "absolute")
        .style("visibility", "hidden")
        .style("padding", "10px")
        .style("background", "var(--primary-color-transparent)")
        .style("border", "1px solid #ddd")
        .style("border-radius", "5px")
        .style("pointer-events", "none")
        .style("text-align", "center")
        .style("color", "var(--text-color)");
        
      // Add dots
      svgContent.append('g')
        .selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
          .attr("cx", d => x(d.x))
          .attr("cy", d => y(d.y))
          .attr("r", 5)
          .style("fill", "#69b3a2")
          .on("mouseover", (event, d) => {
            tooltipDiv.style("visibility", "visible")
                      .html(`${d.name}, ${xaxis_label}: ${d.x}, ${yaxis_label}: ${d.y}`)
                      .style("left", (event.pageX + 10) + "px")
                      .style("top", (event.pageY - 20) + "px");
          })
          .on("mousemove", (event) => {
            tooltipDiv.style("left", (event.pageX + 10) + "px")
                      .style("top", (event.pageY - 20) + "px");
          })
          .on("mouseout", () => {
            tooltipDiv.style("visibility", "hidden");
          });
    }
  }, [data, height, width, title, xaxis_label, yaxis_label]); // Redraw chart if data or size changes

  return (
    <div className='flex-div'>
      <svg ref={d3Container} />
    </div>
  );
}

export default ScatterPlot;