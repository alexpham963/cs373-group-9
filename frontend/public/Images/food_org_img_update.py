import os

dir = "frontend/public/Images/GroceryStoreImages"

# rename images to start from 1 instead of 0
for img in os.listdir(dir):

    num = ""
    for char in img:
        if char.isnumeric():
            num += char

    num = str(int(num) + 1)
    new_img = "store_img" + num + ".png"

    os.rename(dir + "/" + img, dir + "/" + new_img)
