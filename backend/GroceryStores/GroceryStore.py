import PyPDF2

counties = {}


def scrape_pdf(file):
    count = 0
    with open(file, "rb") as pdf_file:
        read_pdf = PyPDF2.PdfReader(pdf_file)
        num_pages = len(read_pdf.pages)

        # iterate through all pages
        for i in range(num_pages):
            page_content = read_pdf.pages[i].extract_text()
            split_info = page_content.split("\n")[1:]

            skip = 0
            for split in split_info:

                if skip > 0:
                    new_split = split.split(" ")

                    # parse line into attributes
                    county = new_split[0].lower()
                    if county not in counties:
                        counties[county] = 1

                    if counties[county] < 4:
                        counties[county] = counties[county] + 1

                        zip = new_split[len(new_split) - 5]

                        new_split = [string for string in new_split if string != ""]
                        split2 = split[len(new_split[0]) :]
                        new_split = [
                            string for string in split2.split(" ") if string != ""
                        ]
                        split2 = split[split.index(new_split[0]) :]
                        new_split2 = split2.split("  ")
                        new_split2 = [string for string in new_split2 if string != ""]
                        name = new_split2[0].strip().lower().replace(",", " ")

                        if len(new_split2) == 4:
                            count += 1
                            addr = new_split2[1].strip().lower().replace(",", " ")
                            city = new_split2[2].strip().lower().replace(",", " ")
                            print(
                                name
                                + ","
                                + county
                                + ","
                                + city
                                + ","
                                + addr
                                + ","
                                + zip
                            )

                skip += 1


# scrape all pdfs
scrape_pdf("backend/GroceryStores/vendor-a-d.pdf")
scrape_pdf("backend/GroceryStores/vendor-e-i.pdf")
scrape_pdf("backend/GroceryStores/vendor-j-l.pdf")
scrape_pdf("backend/GroceryStores/vendor-m-r.pdf")
scrape_pdf("backend/GroceryStores/vendor-s-z.pdf")
