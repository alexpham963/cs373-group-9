import renderer from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';
import { render, screen, fireEvent, cleanup, waitFor } from '@testing-library/react';

import CardContainer from '../components/CardContainer.js';
import InstanceCard from '../components/InstanceCard.js';
import InstancePage from '../components/InstancePage.js';
import Navbar from '../components/Navbar.js';
import About from '../pages/About.js';
import { totalCommits, totalIssues } from '../pages/About.js';
import Home from '../pages/Home.js';
import ModelPage from '../pages/ModelPage.js';
import App from '../App.js'

afterEach(() => {cleanup();});

it('Generate App', () => {
    const genApp = renderer.create(<App />);
    let jsnApp = genApp.toJSON();
    expect(jsnApp).toMatchSnapshot();
});

it('Generate NavBar', () => {
    const genBar = renderer.create(<BrowserRouter><Navbar /></BrowserRouter>,);
    let jsnBar = genBar.toJSON();
    expect(jsnBar).toMatchSnapshot();
});

it('Generate Home Page', () => {
    const genHome = renderer.create(<BrowserRouter><Home /></BrowserRouter>);
    let jsnHome = genHome.toJSON();
    expect(jsnHome).toMatchSnapshot();
});


it('Generate About Page', () => {
    const genAbout = renderer.create(<BrowserRouter><About /></BrowserRouter>);
    let jsnAbout = genAbout.toJSON();
    expect(jsnAbout).toMatchSnapshot();
});
// })

it('Initialize ModelPage', () => {
    const genMP = renderer.create(<BrowserRouter><ModelPage /></BrowserRouter>);
    let jsnMP = genMP.toJSON();
    expect(jsnMP).toMatchSnapshot();
});

it('Initialize InstancePage', () => {
    const genIP = renderer.create(<BrowserRouter><InstancePage /></BrowserRouter>);
    let jsnIP = genIP.toJSON();
    expect(jsnIP).toMatchSnapshot();
});

test('Found Home Page', async () => {
    render(<BrowserRouter><Home /></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText(/Texas Food Insecurity/)).toBeInTheDocument();
    });
});

test('Found About Page', async () => {
    render(<BrowserRouter><About /></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText(/Fighting Food Insecurity in Texas/)).toBeInTheDocument();
    });
});

test('Getting into Charity Organizations', async () => {
    render(<BrowserRouter><ModelPage modelType="Charity Organizations"/></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText('Charity Organizations')).toBeInTheDocument();
    });

    // const charInst = screen.getByText('https://www.texasfoodinsecurity.me/charity_organizations/4');
    // fireEvent.click(charInst);
    
    // await waitFor(() => {
    //     expect(screen.getByText('Name: Coastal Bend Food Bank')).toBeInTheDocument();
    // });
});

// test('Getting into Grocery Stores', async () => {
//     render(<BrowserRouter><ModelPage modelType="Grocery Stores"/></BrowserRouter>);
//     await waitFor(() => {
//         expect(screen.getByText('Grocery Stores')).toBeInTheDocument();
//     });
// });

test('Getting into Counties', async () => {
    render(<BrowserRouter><ModelPage modelType="Counties"/></BrowserRouter>);
    await waitFor(() => {
        expect(screen.getByText('Counties')).toBeInTheDocument();
    });
});
