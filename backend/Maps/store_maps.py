from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import time

# Set up and get web page
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
chrome_options.add_argument("--disable-blink-features=AutomationControlled")
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

url = "https://www.google.com/maps"
counties = open("backend/GroceryStores/GroceryStore4.txt", "r")
county_maps = open("frontend/public/Maps/store_maps3.txt", "w")

counter = 0
prev_county = ""
county_counter = 0

for line in counties:

    if counter > 0:
        driver.get(url)
        time.sleep(2)

        split = line.split(",")
        name = split[0]
        county = split[1]

        if county != prev_county:
            county_counter = 0
            prev_county = county

        if county == prev_county and county_counter < 5:

            if "#" in name:
                ind = name.index("#")
                name = name[:ind]
            search = name + " " + split[2] + " " + split[3]

            try:
                search_box = driver.find_element(
                    By.XPATH,
                    "/html/body/div[1]/div[3]/div[8]/div[3]/div[1]/div[1]/div/div[2]/form/input",
                )
                search_box.send_keys(search)
                search_box.send_keys(Keys.ENTER)

                time.sleep(1)
                share_button = driver.find_element(
                    By.XPATH,
                    "/html/body/div[1]/div[3]/div[8]/div[9]/div/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[5]/button",
                )
                share_button.click()

                time.sleep(1)
                embed_button = driver.find_element(
                    By.XPATH,
                    "/html/body/div[1]/div[3]/div[1]/div/div[2]/div/div[2]/div/div/div/div[2]/button[2]",
                )
                embed_button.click()

                time.sleep(1)
                iframe = driver.find_element(
                    By.XPATH,
                    "/html/body/div[1]/div[3]/div[1]/div/div[2]/div/div[2]/div/div/div/div[3]/div[1]/input",
                ).get_attribute("value")

                county_maps.write(iframe)
                county_maps.write("\n")
                county_counter += 1

            except NoSuchElementException:
                county_maps.write("\n")
                print(counter)

    counter += 1
    # print(counter)
