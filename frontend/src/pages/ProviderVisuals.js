import React, { useEffect, useState } from "react";
import { getDevData } from "../utils/utils";
import ScatterPlot from "../components/ScatterPlot";
import BarChart from "../components/BarChart";
import PieChart from "../components/PieChart";
import { toTitleCase } from "../utils/utils";

const ProviderVisuals = () => {

  const [scatterplot_data, setScatterData] = useState([]);
  const [bargraph_data, setBarData] = useState([]);
  const [piechart_data, setPieData] = useState([]);


  useEffect(() => {
    getDevData("get-all-affordable-housing").then((modelData) => {
      //pie chart
      let numOfcommunityDisabled = 0;
      let numOfcommunitynoDisabled = 0;
      for (let item of modelData) {
        if(item.communityDisabled === "True") {
            numOfcommunityDisabled ++;
        } else {
          numOfcommunitynoDisabled ++;
        }
      }
      setPieData([
        { name: "Supports Disabilities", value: numOfcommunityDisabled },
        { name: "No Disability Support", value: numOfcommunitynoDisabled },
      ]);
    //  Pie chart  

    });

   getDevData("get-all-food-pantries").then((modelData) => {
     // Calculate the average number of ratings and the average rating across all items
     const totalRatings = modelData.reduce(
       (acc, item) => acc + item.numRatings,
       0
     );
     const totalRatingScore = modelData.reduce(
       (acc, item) => acc + item.rating * item.numRatings,
       0
     );
     const averageRating = totalRatings ? totalRatingScore / totalRatings : 0;
     const averageNumRatings = modelData.length
       ? totalRatings / modelData.length
       : 0;

     // Calculate the weighted rating for each item
     modelData.forEach((item) => {
       const weightedRating =
         (item.numRatings / (item.numRatings + averageNumRatings)) *
           item.rating +
         (averageNumRatings / (item.numRatings + averageNumRatings)) *
           averageRating;
       item.weightedRating = weightedRating;
     });

     // Sort by weighted rating in descending order and get the top 10
     const topTen = modelData
       .sort((a, b) => b.numRatings - a.numRatings)
       .slice(0, 10)
       .map((item) => ({
         name: `${item.name}, ${item.numRatings} Reviews`,
         value: item.rating,
         numRatings: item.numRatings,
       }));
      setBarData(topTen)

     // Top ten food pantries with the best ratio between number of ratings and rating
     // Bar chart
   });
   
   getDevData("get-all-thrifts").then((modelData) => {
    let scatterplot_data = [];
    for (let thrift of modelData) {
        scatterplot_data.push({
            x: thrift['numRatings'], 
            y: thrift['rating'],
            name: toTitleCase(thrift['name'])
        })
    }
    setScatterData(scatterplot_data)
   });

  }, []);

  return (
    <div className="page-background visuals">
      <ScatterPlot
        data={scatterplot_data}
        title="Thrift Store Rating vs Number of Ratings"
        xaxis_label="Number of Ratings"
        yaxis_label="Rating"
      />
      <BarChart
        data={bargraph_data}
        title="Rating of Top 10 Most Reviewed Food Banks"
        xaxis_label="Rating"
      ></BarChart>
      <PieChart
        data={piechart_data}
        title="Housing for People with Disabilities"
      ></PieChart>
      <div className='center-text' style={{marginLeft: '0'}}>
          <h1 style={{ padding: "0" }}>
            Developer Critiques
          </h1>
          The website is easy to navigate and has a good amount of data. The descriptions on their model
          pages helped us understand what some of the attributes were. Additionally, their RESTful API was easy to use 
          and gave us the information we needed to construct our visualizations very quickly. They implemented 
          all our user stories well. 
          We were about to learn about low-income communities and what options they might have 
          for food, clothing, and housing by using their website. Some things that could be improved is their dark mode,
          since it currently only applies to the home page and changes only the background color. 
          Another improvement to consider is the scaling and overflows of the model and instance pages for mobile viewing.
          Another change could be to the about page, since it looks relatively plain
          compared to the rest of the site. Finally, one thing that puzzles us is that some of the images displayed on the instance
          cards do not seem very representative of the instance.
        </div>
    </div>
  );
}
export default ProviderVisuals;
