from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import unittest


class myTests(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")  # Run Chrome in headless mode.
        chrome_options.add_argument(
            "--no-sandbox"
        )  # Bypass OS security model, crucial for Docker/container environments.
        chrome_options.add_argument(
            "--disable-dev-shm-usage"
        )  # Overcome limited resource problems.
        chrome_options.add_argument(
            "--disable-gpu"
        )  # Disable GPU hardware acceleration, if applicable.
        chrome_options.add_experimental_option(
            "detach", True
        )  # Keep the browser open after the script ends.

        # Ensure ChromeDriver is up to date and set the path automatically
        service = Service(ChromeDriverManager().install())

        # Initialize the driver with the service and options defined above
        self.driver = webdriver.Chrome(service=service, options=chrome_options)

    def test0(self):
        self.driver.get("https://www.texasfoodinsecurity.me/")
        link = self.driver.find_element(
            By.XPATH, "/html/body/div/div/nav/div/div/div[1]/a[2]"
        ).get_attribute("href")
        try:
            self.assertEqual("https://www.texasfoodinsecurity.me/about", link)
            print("About Page Found")
        except AssertionError:
            print("About Page Not Found")

    def test1(self):
        self.driver.get("https://www.texasfoodinsecurity.me/")
        link = self.driver.find_element(
            By.XPATH, "/html/body/div/div/nav/div/div/div[1]/a[3]"
        ).get_attribute("href")
        try:
            self.assertEqual(
                "https://www.texasfoodinsecurity.me/charity_organizations", link
            )
            print("Charity Orgs Found")
        except AssertionError:
            print("Charity Orgs Not Found")

    def test2(self):
        self.driver.get("https://www.texasfoodinsecurity.me/")
        link = self.driver.find_element(
            By.XPATH, "/html/body/div/div/nav/div/div/div[1]/a[4]"
        ).get_attribute("href")
        try:
            self.assertEqual("https://www.texasfoodinsecurity.me/grocery_stores", link)
            print("Grocery Store Page Found")
        except AssertionError:
            print("Grocery Store Page Not Found")

    def test3(self):
        self.driver.get("https://www.texasfoodinsecurity.me/")
        link = self.driver.find_element(
            By.XPATH, "/html/body/div/div/nav/div/div/div[1]/a[6]"
        ).get_attribute("href")
        try:
            self.assertEqual("https://www.texasfoodinsecurity.me/visualizations", link)
            print("Visuals Page Found")
        except AssertionError:
            print("Visuals Page Not Found")

    def test4(self):
        self.driver.get("https://www.texasfoodinsecurity.me/counties")
        link = self.driver.find_element(
            By.XPATH, "/html/body/div/div/nav/div/div/div[1]/a[1]"
        ).get_attribute("href")
        try:
            self.assertEqual("https://www.texasfoodinsecurity.me/", link)
            print("Home Page Found")
        except AssertionError:
            print("Home Page Not Found")

    def test5(self):
        self.driver.get("https://www.texasfoodinsecurity.me/about")
        self.driver.implicitly_wait(2)
        self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[6]/div/div/div/li[1]/a"
        ).click()
        try:
            self.assertEqual(
                "https://www.homelessshelterdirectory.org/foodbanks/state/texas",
                self.driver.current_url,
            )
            print("Homeless Shelter Directory Site Found")
        except AssertionError:
            print("Homeless Shelter Directory Site Not Found")

    def test6(self):
        self.driver.get("https://www.texasfoodinsecurity.me/charity_organizations")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[2]/div/button"
        ).text
        try:
            self.assertEqual(text, "Filter By")
            print("Orgs Filter Found")
        except AssertionError:
            print("Orgs Filter Not Found")

    def test7(self):
        self.driver.get("https://www.texasfoodinsecurity.me/")
        link = self.driver.find_element(
            By.XPATH, "/html/body/div/div/nav/div/div/div[1]/a[5]"
        ).get_attribute("href")
        try:
            self.assertEqual("https://www.texasfoodinsecurity.me/counties", link)
            print("Counties Page Found")
        except AssertionError:
            print("Counties Page Not Found")

    def test8(self):
        self.driver.get("https://www.texasfoodinsecurity.me/counties")
        self.driver.implicitly_wait(2)
        self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[3]/div/a[2]"
        ).click()
        self.driver.implicitly_wait(2)
        try:
            self.assertEqual(
                "https://www.texasfoodinsecurity.me/counties/2", self.driver.current_url
            )
            print("Andrews County Instance Found")
        except AssertionError:
            print("Andrews County Instance Not Found or Missing/Incorrect Correlation")

    def test9(self):
        self.driver.get("https://www.texasfoodinsecurity.me/about")
        self.driver.find_element(By.XPATH, "/html/body/div/div/nav/a").click()
        try:
            self.assertEqual(
                "https://www.texasfoodinsecurity.me/", self.driver.current_url
            )
            print("Website Title Goes to Home Page")
        except AssertionError:
            print("Website Title Doesn't Properly Go To Home Page")

    def test10(self):
        self.driver.get("https://www.texasfoodinsecurity.me/about")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[4]/h1[1]"
        ).text
        try:
            self.assertEqual(text, "Total Commits: 0")
            print("Commit Info Found")
        except AssertionError:
            print("Commit Info Not Found")

    def test11(self):
        self.driver.get("https://www.texasfoodinsecurity.me/about")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[4]/h1[2]"
        ).text
        try:
            self.assertEqual(text, "Total Issues: 0")
            print("Issue Info Found")
        except AssertionError:
            print("Issue Info Not Found")

    def test12(self):
        self.driver.get("https://www.texasfoodinsecurity.me/charity_organizations")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[1]/div[1]/button"
        ).text
        try:
            self.assertEqual(text, "Sort By")
            print("Charity Org Sort Found")
        except AssertionError:
            print("Charity Org Sort Not Found")

    def test13(self):
        self.driver.get("https://www.texasfoodinsecurity.me/grocery_stores")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[1]/div[1]/button"
        ).text
        try:
            self.assertEqual(text, "Sort By")
            print("Grocery Store Sort Found")
        except AssertionError:
            print("Grocery Store Sort Not Found")

    def test14(self):
        self.driver.get("https://www.texasfoodinsecurity.me/counties")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[1]/div[1]/button"
        ).text
        try:
            self.assertEqual(text, "Sort By")
            print("County Sort Found")
        except AssertionError:
            print("county Sort Not Found")

    def test15(self):
        self.driver.get("https://www.texasfoodinsecurity.me/counties")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[2]/div[1]/button"
        ).text
        try:
            self.assertEqual(text, "Filter By")
            print("County Filter Found")
        except AssertionError:
            print("county Filter Not Found")

    def test16(self):
        self.driver.get("https://www.texasfoodinsecurity.me/charity_organizations")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[3]/form/input"
        ).get_attribute("placeholder")
        try:
            self.assertEqual(text, "Enter Search")
            print("Charity Org Search Found")
        except AssertionError:
            print("Charity Org Search Not Found")

    def test17(self):
        self.driver.get("https://www.texasfoodinsecurity.me/grocery_stores")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[3]/form/input"
        ).get_attribute("placeholder")
        try:
            self.assertEqual(text, "Enter Search")
            print("Grocery Store Search Found")
        except AssertionError:
            print("Grocery Store Search Not Found")

    def test18(self):
        self.driver.get("https://www.texasfoodinsecurity.me/counties")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/div[3]/form/input"
        ).get_attribute("placeholder")
        try:
            self.assertEqual(text, "Enter Search")
            print("County Search Found")
        except AssertionError:
            print("County Search Not Found")

    def test19(self):
        self.driver.get("https://www.texasfoodinsecurity.me/search")
        text = self.driver.find_element(
            By.XPATH, "/html/body/div/div/div/div[2]/form/input"
        ).get_attribute("placeholder")
        try:
            self.assertEqual(text, "Enter Search")
            print("Overall Search Found")
        except AssertionError:
            print("Overall Search Not Found")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
