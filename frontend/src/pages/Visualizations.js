import React, { useEffect, useState } from 'react';
import { getModelData } from '../utils/utils';
import ScatterPlot from '../components/ScatterPlot';
import BarChart from '../components/BarChart';
import '../styles/Visualizations.css'
import { toTitleCase } from '../utils/utils';
import PieChart from '../components/PieChart';

const Visualizations = () => {

    const [scatterplot_data, setScatterData] = useState([]);
    const [bargraph_data, setBarData] = useState([]);
    const [piechart_data, setPieData] = useState([]);

    useEffect(() => {
        getModelData('county').then((modelData) => {
            let scatterplot_data = [];
            for (let county_data of modelData) {
                scatterplot_data.push({
                    x: county_data['total_population']/1000, 
                    y: county_data['num_people_insecure']/1000,
                    name: toTitleCase(county_data['name'])
                })
            }
            setScatterData(scatterplot_data)
        });
        getModelData('full_foodorg').then((foodorg_data) => {
            let foodorg_countyNumDict = {};
            for (let item of foodorg_data) {
                    if(item.is_bank) {
                            if (foodorg_countyNumDict[item.name]) {
                                foodorg_countyNumDict[item.name]++;
                            } else {
                                foodorg_countyNumDict[item.name] = 1;
                            }
                    }
            }
            for (let key in foodorg_countyNumDict) {
                if (foodorg_countyNumDict[key] === 1) {
                    delete foodorg_countyNumDict[key];
                }
            }

            const sortedData = Object.entries(foodorg_countyNumDict)
                .sort((a, b) => b[1] - a[1])
                .map(([name, value]) => ({ name, value }));
            setBarData(sortedData);
        });
        getModelData('grocery').then((grocery_data) => {
          let snap_eligible_counter = 0;
          let non_eligible_counter = 0;
          for (let item of grocery_data) {
            if (item.is_snap) {
              snap_eligible_counter++;
            } else {
              non_eligible_counter++;
            }
          }
          const piechartdata = [
            { name: "Snap Eligible", value: snap_eligible_counter },
            { name: "Non-Eligible", value: non_eligible_counter },
          ];
          setPieData(piechartdata);
        });
    }, [])

    return (
      <div className="page-background visuals">
        <ScatterPlot
          data={scatterplot_data}
          title="County Total Population vs. # of People Insecure (Thousands)"
          xaxis_label="Population"
          yaxis_label="People Insecure"
        />
        <BarChart
          data={bargraph_data}
          title="Top Food Banks that Serves Multiple Counties"
          xaxis_label="# of Counties"
          yaxis_label=""
        ></BarChart>
        <PieChart
          data={piechart_data}
          title="Grocery Stores That Are Snap Eligible"
        ></PieChart>
        <div className='center-text' style={{ marginLeft: '0%' }}>
          <h1 style={{ padding: "0" }}>
            Self Critiques
          </h1>
          Our team worked effectively as a group. We had constant communication
          throughout the entire process and were clear on the tasks each of us needed
          to do. We learned to use many common tools, such as React, Flask, Docker, AWS,
          GitLab, Postman, Selenium, and more. We gave each other guidance on picking up each
          of these tools; people that had prior experience with some helped the others learn how
          to use them. Some possible improvements to the website could include better styling, 
          improved searching, and better page loading speed.
          Finally, some things that puzzle us was mostly learning how to style the frontend as we did not have much experience, or how we might gather even more details about each of our instances.
          Overall more experience and time with web development is required from each team member to improve
          these critiques. Though there is still much to learn, our team has proceeded onward.
          The peer reviews encouraged us all and gave confidence that we were on the right track.
          From this, each members efforts and contribution have been recognized.
        </div>
      </div>
    );
}

export default Visualizations;
