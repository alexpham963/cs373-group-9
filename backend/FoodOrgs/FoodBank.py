from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import time

# Set up and get web page
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
url = "https://www.feedingtexas.org/zip/CODE/help/"

file = open("backend/GroceryStores/GroceryStore.txt", "r")
for line in file:

    # Get zip code from file
    data = line.split(",")
    zip_code = data[len(data) - 1]

    driver.get(url.replace("CODE", zip_code))
    time.sleep(1)
    bank = driver.find_element(
        By.XPATH, "/html/body/div[1]/div[1]/main/div/section/div[1]/h2/a"
    )

    print(data[1] + "," + data[0] + "," + bank.get_attribute("text"))
