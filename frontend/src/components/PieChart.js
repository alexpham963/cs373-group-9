import React, { useRef, useEffect } from 'react';
import * as d3 from 'd3';

const PieChart = ({ data, title='' }) => {
  const d3Container = useRef(null);

  useEffect(() => {
    if (data && d3Container.current) {
        const svg = d3.select(d3Container.current);

        // Clear the SVG to avoid duplication
        svg.selectAll("*").remove();

        const margin = { top: 50, right: 0, bottom: 50, left: 0 };
        const width = 400;
        const height = 400;
        const radius = Math.min(width, height) / 2;

        const pie = d3.pie().value(d => d.value)(data);
        const arc = d3.arc().innerRadius(0).outerRadius(radius);

        // Create SVG element
        const svgContent = svg
            .attr('width', width+margin.left+margin.right)
            .attr('height', height+margin.top+margin.bottom)
            .append('g')
            .attr("transform", `translate(${width/2+margin.left}, ${height/2+margin.top})`);

        const tooltipDiv = d3.select(d3Container.current.parentNode)
            .append("div")
            .style("position", "absolute")
            .style("top", "0px")
            .style("visibility", "hidden")
            .style("padding", "10px")
            .style("background", "var(--primary-color-transparent)")
            .style("border", "1px solid #ddd")
            .style("border-radius", "5px")
            .style("pointer-events", "none")
            .style("text-align", "center")
            .style("color", "var(--text-color)");
    
        // Build the pie chart
        svgContent.selectAll('path')
            .data(pie)
            .enter()
            .append('path')
            .attr('d', arc)
            .attr('fill', (d, i) => d3.schemeTableau10[i])
            .attr('stroke', 'white')
            .style('stroke-width', '2px')
            .on("mouseover", (event, d) => {
                tooltipDiv.style("visibility", "visible")
                          .html(`${d.data.name}: ${d.data.value}`)
                          .style("left", (event.pageX + 10) + "px")
                          .style("top", (event.pageY - 20) + "px");
              })
              .on("mousemove", (event) => {
                tooltipDiv.style("left", (event.pageX + 10) + "px")
                          .style("top", (event.pageY - 20) + "px");
              })
              .on("mouseout", () => {
                tooltipDiv.style("visibility", "hidden");
              });

        // Add labels
        svgContent.selectAll('text')
            .data(pie)
            .enter()
            .append('text')
            .text(d => `${d.data.name}: ${d.data.value}`)
            .attr('transform', d => `translate(${arc.centroid(d)})`)
            .style('text-anchor', 'middle')
            .style('font-size', 12);

        //title
        svgContent.append("text")
            .attr("x", 0)
            .attr("y", -height/2-margin.top/2)
            .attr("text-anchor", "middle")
            .style("font-size", "20px")
            .style("text-decoration", "underline")
            .style("fill", "var(--text-color)")
            .text(title);
        
    }    
  }, [data, title]); // Redraw chart if data changes

  return (
    <div className='flex-div'>
      <svg ref={d3Container} />
    </div>
  );
};

export default PieChart;