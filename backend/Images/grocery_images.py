import requests
from bs4 import BeautifulSoup
from PIL import Image

file = open("backend/GroceryStores/GroceryStore5.txt")
links = open("backend/Images/GroceryStoreImages.txt", "w")
counter = 1

for line in file:

    print(counter)

    # easy access links for very common store names
    if "kroger" in line:
        img_url = "https://prnewswire2-a.akamaihd.net/p/1893751/sp/189375100/thumbnail/entry_id/0_kwshpnaa/def_height/2700/def_width/2700/version/100012/type/1"

    elif "brookshire" in line:
        img_url = "https://storage.googleapis.com/images-bks-prd-1385851.bks.prd.v8.commerce.mi9cloud.com/bks/img/Brookshires-Logo.webp"

    elif "heb" in line:
        img_url = "https://upload.wikimedia.org/wikipedia/en/thumb/a/a1/H-E-B_logo.svg/1200px-H-E-B_logo.svg.png"

    elif "wal-mart" in line or "walmart" in line:
        img_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Walmart_logo.svg/2560px-Walmart_logo.svg.png"

    elif "lowe" in line:
        img_url = "https://upload.wikimedia.org/wikipedia/en/thumb/e/e5/Lowe%27s_Market_logo.svg/800px-Lowe%27s_Market_logo.svg.png"

    elif "albertson" in line:
        img_url = "https://fiu-original.b-cdn.net/fontsinuse.com/use-images/23/23998/23998.png?filename=albertsons-logo.png"

    elif "united supermarket" in line:
        img_url = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDuG9B5djeNJV5_kfDIAetnil7B5tvBINZBC7S4Tx-Ng&s"

    # otherwise create unique search term
    else:
        info = line.split(",")
        search = info[0] + " " + info[1] + " " + info[2] + " " + info[3]

        url = "https://www.google.com/search?tbm=isch&q=" + search
        page = requests.get(url)
        soup = BeautifulSoup(page.content, "html.parser")

        # get first image from results
        img = soup.find_all("img")[1]
        img_url = img["src"]

    # save image as png
    img = Image.open(requests.get(img_url, stream=True).raw)
    img.save("backend/Images/GroceryStoreImages2/groc_img" + str(counter) + ".png")
    counter += 1

file.close()
links.close()
