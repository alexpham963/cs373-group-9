import openpyxl
from bs4 import BeautifulSoup
import requests

# Open worksheet
wb = openpyxl.load_workbook("backend//Counties//MMG2023_2019-2021_Data_ToShare.xlsx")
ws = wb["County"]

food_insecurity_rates = []
num_insecure = []
child_food_insecurity_rate = []
num_children_insecure = []
cost_per_meal = []

# Grab information for TX Counties only
for i in range(8811, 9065):
    food_insecurity_rates.append(ws.cell(i, 5).value)
    num_insecure.append(ws.cell(i, 6).value)
    child_food_insecurity_rate.append(ws.cell(i, 18).value)
    num_children_insecure.append(ws.cell(i, 19).value)
    cost_per_meal.append(ws.cell(i, 22).value)

# Scrape population api for county populations
r = requests.get(
    "https://datausa.io/api/data?drilldowns=County&measures=Population&year=latest"
)

soup = BeautifulSoup(r.content, "html5lib")
string_info = soup.prettify()
counties = string_info.split("}")

population_dict = {}
keys = []
populations = []

# Parse html string into county names and populations
for county in counties:
    if "TX" in county:
        pop_split = county.split("Population")

        for split in pop_split:
            if "ID" not in split:
                split = split[2 : len(split) - 11]
                population = int(split.split(",")[0])
                name = split.split(":")[1][1:]
                populations.append(population)
                keys.append(name)

# Create dictionary with all county information
for i in range(len(keys)):
    values = []
    values.append(populations[i])
    values.append(food_insecurity_rates[i])
    values.append(num_insecure[i])
    values.append(child_food_insecurity_rate[i])
    values.append(num_children_insecure[i])
    values.append(cost_per_meal[i])
    population_dict[keys[i]] = values

print(population_dict)

file = open("CountyInfo.txt", "w")
for key in population_dict:
    string_to_write = key
    for val in population_dict[key]:
        string_to_write += "," + str(val)
    file.write(string_to_write + "\n")
