from flask import Flask, jsonify, request, url_for
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_, func
import requests
from Models import (
    app,
    db,
    FoodOrg,
    GroceryStore,
    County,
    FoodOrg_County,
    FoodOrgSchema,
    FoodOrg_GroceryStore,
    GroceryStoreSchema,
    CountySchema,
    FoodOrg_CountySchema,
    FoodOrg_GroceryStoreSchema,
)

page_default_size = 12


@app.route("/")
def hello():
    return {
        "Message": "Texas Food Security, routes: /foodorg /grocerystores /county, to get all instance append /all ex: /foodorg/all"
    }


@app.route("/get/foodorg", methods=["GET"])
@app.route("/foodorg", methods=["GET"])
def org_get():
    sort_by = request.args.get("sort_by", "id")
    order = request.args.get("order", "asc")
    page_size = request.args.get("page_size", str(page_default_size))
    page = request.args.get("page", "1")

    page_size = int(page_size)
    page = int(page)

    filter = request.args.get("filter", "")

    num_rows = db.session.query(FoodOrg).count()

    valid_attributes = [
        "id",
        "name",
        "address",
        "city",
        "zip_code",
        "phone_number",
        "county",
    ]

    if sort_by not in valid_attributes:
        return jsonify({"error": "Invalid sort attribute"}), 400

    if order == "asc":
        query_results = FoodOrg.query.order_by(getattr(FoodOrg, sort_by).asc())
    elif order == "desc":
        query_results = FoodOrg.query.order_by(getattr(FoodOrg, sort_by).desc())
    else:
        return jsonify({"error": "Invalid order parameter"}), 400

    if filter == "bank":
        query_results = query_results.filter(FoodOrg.is_bank == True)
        num_rows = query_results.count()
    elif filter == "pantry":
        query_results = query_results.filter(FoodOrg.pantry == True)
        num_rows = query_results.count()
    elif filter == "food_kitchen":
        query_results = query_results.filter(FoodOrg.food_kitchen == True)
        num_rows = query_results.count()

    foodorgs = query_results.paginate(page=page, per_page=page_size, error_out=False)
    result = FoodOrgSchema().dump(foodorgs, many=True)
    return jsonify({"foodorg": result, "length": num_rows})


@app.route("/get/county", methods=["GET"])
@app.route("/county", methods=["GET"])
def county_get():
    sort_by = request.args.get("sort_by", "id")
    order = request.args.get("order", "asc")
    page_size = request.args.get("page_size", str(page_default_size))
    page = request.args.get("page", "1")

    filter = request.args.get("filter", "")
    filter_num = request.args.get("filterNum", "0")
    filter_cond = request.args.get("filterCond", "lessThan")

    filter_list = filter.split()
    filter_num_list = filter_num.split()
    filter_cond_list = filter_cond.split()

    page_size = int(page_size)
    page = int(page)

    num_rows = db.session.query(County).count()

    valid_attributes = [
        "id",
        "name",
        "total_population",
        "percent_people_insecure",
        "num_people_insecure",
        "percent_children_insecure",
        "num_children_insecure",
        "cost_per_meal",
    ]

    if sort_by not in valid_attributes:
        return jsonify({"error": "Invalid sort attribute"}), 400

    if order == "asc":
        query_results = County.query.order_by(getattr(County, sort_by).asc())
    elif order == "desc":
        query_results = County.query.order_by(getattr(County, sort_by).desc())
    else:
        return jsonify({"error": "Invalid order parameter"}), 400

    for i in range(0, len(filter_list)):
        if filter_cond_list[i] == "lessThan":
            query_results = query_results.filter(
                getattr(County, filter_list[i]) < filter_num_list[i]
            )
        elif filter_cond_list[i] == "greaterThan":
            query_results = query_results.filter(
                getattr(County, filter_list[i]) > filter_num_list[i]
            )

    counties = query_results.paginate(page=page, per_page=page_size, error_out=False)
    result = CountySchema().dump(counties, many=True)
    return jsonify({"county": result, "length": num_rows})


@app.route("/get/grocerystores", methods=["GET"])
@app.route("/grocerystores", methods=["GET"])
def store_get():
    sort_by = request.args.get("sort_by", "id")
    order = request.args.get("order", "asc")
    page_size = request.args.get("page_size", str(page_default_size))
    page = request.args.get("page", "1")

    page_size = int(page_size)
    page = int(page)

    filter = request.args.get("filter", "")

    num_rows = db.session.query(GroceryStore).count()

    valid_attributes = ["id", "name", "county", "city", "address", "zip_code"]

    if sort_by not in valid_attributes:
        return jsonify({"error": "Invalid sort attribute"}), 400

    if order == "asc":
        query_results = GroceryStore.query.order_by(
            getattr(GroceryStore, sort_by).asc()
        )
    elif order == "desc":
        query_results = GroceryStore.query.order_by(
            getattr(GroceryStore, sort_by).desc()
        )
    else:
        return jsonify({"error": "Invalid order parameter"}), 400

    if filter == "snap":
        query_results = query_results.filter(GroceryStore.is_snap == True)
        num_rows = query_results.count()

    grocerystores = query_results.paginate(
        page=page, per_page=page_size, error_out=False
    )
    result = GroceryStoreSchema().dump(grocerystores, many=True)
    return jsonify({"grocerystores": result, "length": num_rows})


@app.route("/get/foodorg/<int:id>", methods=["GET"])
@app.route("/foodorg/<int:id>", methods=["GET"])
def org_get_instance(id):
    instance = FoodOrg.query.get(id)

    result = FoodOrgSchema().dump(instance, many=False)
    return jsonify({"foodorg": result})


@app.route("/get/county/<int:id>", methods=["GET"])
@app.route("/county/<int:id>", methods=["GET"])
def county_get_instance(id):
    instance = County.query.get(id)

    result = CountySchema().dump(instance, many=False)
    return jsonify({"county": result})


@app.route("/get/grocerystores/<int:id>", methods=["GET"])
@app.route("/grocerystores/<int:id>", methods=["GET"])
def store_get_instance(id):
    instance = GroceryStore.query.get(id)

    result = GroceryStoreSchema().dump(instance, many=False)
    return jsonify({"grocerystores": result})


@app.route("/get/foodorg/all", methods=["GET"])
@app.route("/foodorg/all", methods=["GET"])
def org_get_all():
    sort_by = request.args.get("sort_by", "id")
    order = request.args.get("order", "asc")
    page_size = 1000
    page = request.args.get("page", "1")

    filter = request.args.get("filter", "")

    page_size = int(page_size)
    page = int(page)

    num_rows = db.session.query(FoodOrg).count()

    valid_attributes = [
        "id",
        "name",
        "address",
        "city",
        "zip_code",
        "phone_number",
        "county",
    ]

    if sort_by not in valid_attributes:
        return jsonify({"error": "Invalid sort attribute"}), 400

    if order == "asc":
        query_results = FoodOrg.query.order_by(getattr(FoodOrg, sort_by).asc())
    elif order == "desc":
        query_results = FoodOrg.query.order_by(getattr(FoodOrg, sort_by).desc())
    else:
        return jsonify({"error": "Invalid order parameter"}), 400

    if filter == "bank":
        query_results = query_results.filter(FoodOrg.is_bank == True)
        num_rows = query_results.count()
    elif filter == "pantry":
        query_results = query_results.filter(FoodOrg.pantry == True)
        num_rows = query_results.count()
    elif filter == "food_kitchen":
        query_results = query_results.filter(FoodOrg.food_kitchen == True)
        num_rows = query_results.count()

    foodorgs = query_results.paginate(page=page, per_page=page_size, error_out=False)
    result = FoodOrgSchema().dump(foodorgs, many=True)
    return jsonify({"foodorg": result, "length": num_rows})


@app.route("/get/county/all", methods=["GET"])
@app.route("/county/all", methods=["GET"])
def county_get_all():
    sort_by = request.args.get("sort_by", "id")
    order = request.args.get("order", "asc")
    page_size = 1000
    page = request.args.get("page", "1")

    filter = request.args.get("filter", "")
    filter_num = request.args.get("filterNum", "0")
    filter_cond = request.args.get("filterCond", "lessThan")

    filter_list = filter.split()
    filter_num_list = filter_num.split()
    filter_cond_list = filter_cond.split()

    page_size = int(page_size)
    page = int(page)

    num_rows = db.session.query(County).count()

    valid_attributes = [
        "id",
        "name",
        "total_population",
        "percent_people_insecure",
        "num_people_insecure",
        "percent_children_insecure",
        "num_children_insecure",
        "cost_per_meal",
    ]

    if sort_by not in valid_attributes:
        return jsonify({"error": "Invalid sort attribute"}), 400

    if order == "asc":
        query_results = County.query.order_by(getattr(County, sort_by).asc())
    elif order == "desc":
        query_results = County.query.order_by(getattr(County, sort_by).desc())
    else:
        return jsonify({"error": "Invalid order parameter"}), 400

    for i in range(0, len(filter_list)):
        if filter_cond_list[i] == "lessThan":
            query_results = query_results.filter(
                getattr(County, filter_list[i]) < filter_num_list[i]
            )
        elif filter_cond_list[i] == "greaterThan":
            query_results = query_results.filter(
                getattr(County, filter_list[i]) > filter_num_list[i]
            )

    counties = query_results.paginate(page=page, per_page=page_size, error_out=False)
    result = CountySchema().dump(counties, many=True)
    return jsonify({"county": result, "length": num_rows})


@app.route("/get/grocerystores/all", methods=["GET"])
@app.route("/grocerystores/all", methods=["GET"])
def store_get_all():
    sort_by = request.args.get("sort_by", "id")
    order = request.args.get("order", "asc")
    page_size = 1000
    page = request.args.get("page", "1")

    page_size = int(page_size)
    page = int(page)

    filter = request.args.get("filter", "")

    num_rows = db.session.query(GroceryStore).count()

    valid_attributes = ["id", "name", "county", "city", "address", "zip_code"]

    if sort_by not in valid_attributes:
        return jsonify({"error": "Invalid sort attribute"}), 400

    if order == "asc":
        query_results = GroceryStore.query.order_by(
            getattr(GroceryStore, sort_by).asc()
        )
    elif order == "desc":
        query_results = GroceryStore.query.order_by(
            getattr(GroceryStore, sort_by).desc()
        )
    else:
        return jsonify({"error": "Invalid order parameter"}), 400

    if filter == "snap":
        query_results = query_results.filter(GroceryStore.is_snap == True)
        num_rows = query_results.count()

    grocerystores = query_results.paginate(
        page=page, per_page=page_size, error_out=False
    )
    result = GroceryStoreSchema().dump(grocerystores, many=True)
    return jsonify({"grocerystores": result, "length": num_rows})


@app.route("/foodorg_county/all", methods=["GET"])
def foodorg_county_get_all():
    num_rows = db.session.query(FoodOrg_County).count()
    query_results = db.session.query(FoodOrg_County).all()
    result = FoodOrg_CountySchema().dump(query_results, many=True)
    return jsonify({"foodorg_counties": result, "length": num_rows})


@app.route("/foodorg_grocerystore/all", methods=["GET"])
def foodorg_grocerystore_get_all():
    num_rows = db.session.query(FoodOrg_GroceryStore).count()
    query_results = db.session.query(FoodOrg_GroceryStore).all()
    result = FoodOrg_GroceryStoreSchema().dump(query_results, many=True)
    return jsonify({"foodorg_grocerystore": result, "length": num_rows})


def get_query_conditions(term_list, model):
    frequency_graph = {}

    for term in term_list:
        query_conditions = []
        for c in model.__table__.columns:
            if str(c.type) == "VARCHAR":
                query_conditions.append(c.ilike(f"%{term}%"))
            elif str(c.type) == "INTEGER" and is_convertible_to_int(term):
                query_conditions.append(c == int(term))
            elif str(c.type) == "FLOAT" and is_convertible_to_float(term):
                query_conditions.append(c == float(term))

        if len(query_conditions) != 0:
            my_query = model.query.filter(or_(*query_conditions))
            for row in my_query:
                if row not in frequency_graph:
                    frequency_graph[row] = 1
                else:
                    frequency_graph[row] += 1

    return frequency_graph


@app.route("/search/<string:model>", methods=["GET"])
def search(model):
    search_terms = request.args.get("terms", "")
    search_terms = search_terms.lower()
    search_list = search_terms.split()

    if model == "grocerystore":
        my_model = GroceryStore
        schema = GroceryStoreSchema()
        pass
    elif model == "county":
        my_model = County
        schema = CountySchema()
        pass
    elif model == "foodorg":
        my_model = FoodOrg
        schema = FoodOrgSchema()
        pass
    else:
        return "error"

    frequency_graph = get_query_conditions(search_list, my_model)

    data = sorted(
        frequency_graph.keys(), key=lambda item: frequency_graph[item], reverse=True
    )
    result = schema.dump(data, many=True)

    return jsonify({"data": result, "length": len(data)})


@app.route("/search", methods=["GET"])
def search_all():
    search_terms = request.args.get("terms", "")
    search_list = search_terms.split()

    full_term = ""
    for term in search_list:
        full_term += term
    search_list.append(full_term)

    model_list = [
        (GroceryStore, GroceryStoreSchema()),
        (County, CountySchema()),
        (FoodOrg, FoodOrgSchema()),
    ]

    result = []

    for item in model_list:
        frequency_graph = get_query_conditions(search_list, item[0])

        data = sorted(
            frequency_graph.keys(), key=lambda item: frequency_graph[item], reverse=True
        )
        result.append(item[1].dump(data, many=True))

    return jsonify(
        {
            "grocerydata": result[0],
            "grocerylength": len(result[0]),
            "countydata": result[1],
            "countylength": len(result[1]),
            "foodorgdata": result[2],
            "foodorglength": len(result[2]),
        }
    )


def is_convertible_to_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def is_convertible_to_float(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
