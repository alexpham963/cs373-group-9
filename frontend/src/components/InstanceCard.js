import React from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import "../styles/InstanceCard.css"
import { toTitleCase, filterDisplayableAttr, nameAttrFirst, getOriginFromUrl, handleHighlight } from '../utils/utils';

const InstanceCard = ({ cardData, link }) => {
  let shortCardData = Object.fromEntries(
    Object.entries(filterDisplayableAttr(nameAttrFirst(cardData))).slice(0, 10)
  );

  const LinkToInstancePage = () => {
    let origin = getOriginFromUrl(window.location.href);
    return `${origin}/${link}`;
  };

  const modelAttributes = {
    foodorg: ["Name", "Address", "City", "County", "Phone Number", "Zip Code"],
    grocery: ["Name", "Address", "City", "County", "Zip Code"],
    county: ["Name", "Cost Per Meal", "Num Children Insecure", "Num People Insecure", "Percent Children Insecure", "Percent People Insecure", "Total Population"],
  };

  let modelName = "";
  if (link.split("/")[0] === "charity_organizations") {
    modelName = "foodorg";
  } else if (link.split("/")[0] === "grocery_stores") {
    modelName = "grocery";
  } else {
    modelName = "county"
  }



  return (
    <Link style={{ textDecoration: "none" }} to={LinkToInstancePage()}>
      <Card className="instance-card">
        <Card.Body
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <div className="img-container">
            <img
              src={cardData["Image"]}
              alt=""
              style={{ width: "100%", height: "100%", objectFit: "contain" }}
            ></img>
          </div>
          {Object.entries(shortCardData).map(([key, value]) => {
            key = toTitleCase(key.toString().replaceAll("_", " "));
            if (
              modelAttributes[modelName].includes(key) &&
              value.toString() !== ""
            ) {
              value = toTitleCase(value);
              if (key === "Name") {
                value = handleHighlight(value);
                return (
                  <div key={key} className="name">
                    {value}
                  </div>
                );
              } else {
                let text = handleHighlight(`${key}: ${value}`);
                return <div key={key}>{text}</div>;
              }
            } else {
              return null; // Don't display anything for disallowed attributes or empty values
            }
          })}
          <div>Click for more info</div>
        </Card.Body>
      </Card>
    </Link>
  );
};
export default InstanceCard;