import { React, useState, useEffect} from 'react';
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import CardContainer from './CardContainer';
import '../styles/InstancePage.css'
import { filterDisplayableAttr, toTitleCase, nameAttrFirst, isEmpty, getModelData, getInstanceData } from '../utils/utils';

const getRelatedData = async (currentInstance) => {
  let modelType = new URL(window.location.href).pathname.split('/')[1];
  let relatedData = []
  if (modelType.toLowerCase().startsWith('charity')) {

    let foodorg_county_data = await getModelData('foodorg_county');
    let county_data = await getModelData('county');
    let foodorg_data = await getModelData('full_foodorg');
    let foodorg_grocerystore_data = await getModelData("foodorg_grocerystore");
    let grocery_data = await getModelData('grocery')
    let foodorg_name = ""
    for (let item of foodorg_data) {
      if (item.id === currentInstance.id) {
        foodorg_name = item.name
      }
    }

    let countySet = new Set()
    for (let item of foodorg_data) {
      if (foodorg_name === item.name) {
        for (let item1 of foodorg_county_data) {
          if (item.id === item1.foodorg_id) {
            countySet.add(item1.county_id)
          }
        }
      }
    }

    let grocerySet = new Set()
    for (let item of foodorg_data) {
      if (foodorg_name === item.name) {
        for (let item1 of foodorg_grocerystore_data) {
          if (item.id === item1.foodorg_id) {
            grocerySet.add(item1.grocerystore_id)
          }
        }
      }
    }


    for (let item of county_data) {
      if (countySet.has(item.id)) {
        relatedData.push(item)
      }
    }
    
    for (let item of grocery_data) {
      if (grocerySet.has(item.id)) {
        relatedData.push(item)
      }
    }

  } else if(modelType.toLowerCase().startsWith('grocery')) {
    let foodorg_grocerystore_data = await getModelData("foodorg_grocerystore");
    let foodorg_set = new Set()
    let foodorg_data = await getModelData("full_foodorg");
    for (let item of foodorg_grocerystore_data) {
      if (currentInstance.id === item.grocerystore_id) {
        foodorg_set.add(item.foodorg_id);
      }
    }
    for (let item of foodorg_data) {
      if (foodorg_set.has(item.id)) {
        relatedData.push(item)
      }
    }
    relatedData.push(await getInstanceData(currentInstance.county_id, "counties"))
  } else if(modelType.toLowerCase().startsWith('counties')) {
    let foodorg_data = await getModelData("full_foodorg");
    let foodorg_county_data = await getModelData("foodorg_county")
    let foodorg_set = new Set()
    for (let item of foodorg_county_data) {
      if (currentInstance.id === item.county_id) {
        foodorg_set.add(item.foodorg_id)
      }
    }
    for (let item of foodorg_data) {
      if (foodorg_set.has(item.id)) {
        relatedData.push(item);
      }
    }

    let grocery_data = await getModelData('grocery')
    for (let item of grocery_data) {
      if (item.county_id.toString() === currentInstance.id.toString()) {
        relatedData.push(item)
      }
    }
  }
  return relatedData
}

async function readLineFromFile1(modelType, lineNumber) {
  let filePath = "";
  if (modelType.toLowerCase().startsWith('charity')) {
    filePath = `/Maps/org_maps.txt`;
  } else if(modelType.toLowerCase().startsWith('grocery')) {
    filePath = `/Maps/store_maps.txt`;
  } else if(modelType.toLowerCase().startsWith('counties')) {
    filePath = `/Maps/county_maps.txt`;
  }
  const response = await fetch(filePath);
  const text = await response.text();
  const lines = text.split('\n');
  return lines[lineNumber - 1]; // Assuming lineNumber is 1-based
}

const InstancePage = () => {
  const [instanceData, setInstanceData] = useState({});
  const [mapIframe, setMapIframe] = useState('');
  const [relatedData, setRelatedData] = useState([]);
  let { id } = useParams();
  const modelType = new URL(window.location.href).pathname.split('/')[1];

  useEffect(() => {
    getInstanceData(id, modelType)
      .then(data => setInstanceData(data));

    readLineFromFile1(modelType, id)
      .then(line => setMapIframe(line));
    
  } ,[modelType, id])

  useEffect(() => {
    if(!isEmpty(instanceData)) {
      getRelatedData(instanceData)
      .then(relatedData => setRelatedData(relatedData))
    }
  }, [instanceData])

  return (
    <div className="page-background">
      <div className="content-wrapper">
        <div className="large-img-container">
          <img src={instanceData.Image} alt="" className="responsive-img"></img>
        </div>
        <div
          className="instance-card"
          style={{ padding: "4%", whiteSpace: "nowrap" }}
        >
          {Object.entries(filterDisplayableAttr(nameAttrFirst(instanceData)))
            .filter(
              ([key, value]) =>
                key !== "website" && key !== "county_web" && key !== "snap_web"
            )
            .map(([key, value]) => {
              key = toTitleCase(key.toString().replaceAll("_", " "));
              value = toTitleCase(value);
              return (
                <div key={key} className="instance-attr">
                  {key === "Name" ? (
                    <h2 style={{ fontWeight: "bold" }}>{value}</h2>
                  ) : (
                    `${key}: ${value || "False"}`
                  )}
                </div>
              );
            })}
          <Link
            className="back-link"
            to={`${useLocation().pathname.replace(/\/\d+$/, "")}`}
          >
            Back
          </Link>
        </div>
      </div>
      <h1 className="section-title">Map</h1>
      <div
        className="map-iframe-container"
        dangerouslySetInnerHTML={{ __html: mapIframe }}
        style={{ width: "80%", justifyContent: "center", }}
      ></div>
      {instanceData.snap_web !== "None" && (
        <div
          className="instance-card"
          style={{
            padding: "4%",
            minWidth: "40px",
            maxWidth: "40vw",
            // width: "20%",
            margin: "0 auto",
            justifyContent: "center",
          }}
        >
          <h1>Click For Website</h1>
          {instanceData.snap_web ? (
            <div>
              <a
                href={instanceData.snap_web}
                target="_blank"
                rel="noopener noreferrer"
                style={{ wordWrap: "break-word", fontSize: "20px" }}
              >
                <p
                  style={{
                    display: "inline-block",
                    backgroundColor: "var(--section-color-2)",
                    color: "var(--button-color)",
                    padding: "10px 20px",
                    textDecoration: "none",
                    borderRadius: "4px",
                    textAlign: "center",
                    fontWeight: "bold",
                    width: "100%",
                  }}
                >
                  {/* {toTitleCase(instanceData.snap_web)} */}
                  {toTitleCase(instanceData.name) + " Snap Website"}
                </p>
              </a>
            </div>
          ) : instanceData.website ? (
            <a
              href={instanceData.website}
              target="_blank"
              rel="noopener noreferrer"
              style={{ wordWrap: "break-word", fontSize: "20px" }}
            >
              <p
                style={{
                  display: "inline-block",
                  backgroundColor: "var(--section-color-2)",
                  color: "var(--button-color)",
                  padding: "10px 20px",
                  textDecoration: "none",
                  borderRadius: "4px",
                  textAlign: "center",
                  fontWeight: "bold",
                  width: "100%",
                }}
              >
                {/* {toTitleCase(instanceData.website)} */}
                {toTitleCase(instanceData.name) + " Website"}
              </p>
            </a>
          ) : (
            <a
              href={instanceData.county_web}
              target="_blank"
              rel="noopener noreferrer"
              style={{ wordWrap: "break-word", fontSize: "20px" }}
            >
              <p
                style={{
                  display: "inline-block",
                  backgroundColor: "var(--section-color-2)",
                  color: "var(--button-color)",
                  padding: "10px 20px",
                  textDecoration: "none",
                  borderRadius: "4px",
                  textAlign: "center",
                  fontWeight: "bold",
                  width: "100%",
                }}
              >
                {/* {toTitleCase(instanceData.county_web) || "N/A"} */}
                {toTitleCase(instanceData.name) + " County Website" || "N/A"}
              </p>
            </a>
          )}
        </div>
      )}

      {instanceData.snap_web ? (
        <>
          <div className="map-iframe-container" >
            <a
              href="https://www.arcgis.com/apps/webappviewer/index.html?id=15e1c457b56c4a729861d015cd626a23"
              style={{
                display: "inline-block",
                backgroundColor: "var(--section-color-2)",
                color: "var(--button-color)",
                padding: "10px 20px",
                textDecoration: "none",
                borderRadius: "4px",
                textAlign: "center",
                fontWeight: "bold",
              }}
            >
              Open in ArcGIS <br></br> Web AppViewer
            </a>
          </div>
          <iframe
          title="argis"
            src="https://www.arcgis.com/apps/webappviewer/index.html?id=15e1c457b56c4a729861d015cd626a23"
            style={{
              width: "80%",
              height: "inherit",
              border: 0,
              alignSelf: "center",
            }}
            allowFullScreen={true}
            loading="lazy"
            referrerPolicy="no-referrer-when-downgrade"
          ></iframe>
        </>
      ) : null}

      <h1 className="section-title">Related To:</h1>
      <CardContainer data={relatedData}></CardContainer>
    </div>
  );
};
export default InstancePage;
