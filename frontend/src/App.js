import "./App.css";

import { React, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import About from "./pages/About";
import InstancePage from "./components/InstancePage";
import ModelPage from "./pages/ModelPage";
import SearchPage from "./pages/SearchPage";
import Visualizations from "./pages/Visualizations";
import ProviderVisuals from "./pages/ProviderVisuals";

const App = () => {

  useEffect(() => {
    // Set the attribute 'data-theme' to 'dark' on the document element
    document.documentElement.setAttribute('data-theme', 'light');
  }, []); // The empty array ensures this effect runs once on mount

  return (
    <div className="flex-div" style={{ flexDirection: "column", flexGrow: 1 }}>
        <Router>
          <Navbar />
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/about" element={<About/>} />
            <Route path="/charity_organizations" element={<ModelPage modelType="Charity Organizations"/>} />
            <Route path="/grocery_stores" element={<ModelPage modelType="Grocery Stores"/>} />
            <Route path="/counties" element={<ModelPage modelType="Counties"/>} />
            <Route path="/search" element={<SearchPage />} />
            <Route path="/charity_organizations/:id" element={<InstancePage/>} />
            <Route path="/grocery_stores/:id" element={<InstancePage/>} />
            <Route path="/counties/:id" element={<InstancePage/>} />
            <Route path="/visualizations/" element={<Visualizations/>} />
            <Route path="/provider_visuals/" element={<ProviderVisuals/>} />
          </Routes>
        </Router>
    </div>
  );
};

export default App;
