from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import time

# Set up and get web page
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
url = "https://www.google.com/maps"
counties = open("backend/Counties/CountyInfo.txt", "r")
county_maps = open("frontend/public/Maps/county_maps.txt", "w")
for line in counties:

    if "county name" not in line:
        county = line.split(",")[0]
        driver.get(url)
        time.sleep(1)

        search_box = driver.find_element(
            By.XPATH,
            "/html/body/div[1]/div[3]/div[8]/div[3]/div[1]/div[1]/div/div[2]/form/input",
        )
        search_box.send_keys(county + " county, TX")
        search_box.send_keys(Keys.ENTER)

        try:
            time.sleep(1)
            share_button = driver.find_element(
                By.XPATH,
                "/html/body/div[1]/div[3]/div[8]/div[9]/div/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[5]/button",
            )
            share_button.click()

            time.sleep(1)
            embed_button = driver.find_element(
                By.XPATH,
                "/html/body/div[1]/div[3]/div[1]/div/div[2]/div/div[2]/div/div/div/div[2]/button[2]",
            )
            embed_button.click()

            time.sleep(1)
            iframe = driver.find_element(
                By.XPATH,
                "/html/body/div[1]/div[3]/div[1]/div/div[2]/div/div[2]/div/div/div/div[3]/div[1]/input",
            ).get_attribute("value")

            county_maps.write(iframe)
            county_maps.write("\n")

        except NoSuchElementException:
            print(county + ": FIX!!!")
            county_maps.write("")
            county_maps.write("\n")
