import React, { useRef, useEffect } from 'react';
import * as d3 from 'd3';

const BarChart = ({ data, xaxis_label='', title='' }) => {
  const d3Container = useRef(null);

  useEffect(() => {
    if (data && d3Container.current) {
        const svg = d3.select(d3Container.current);

        // Clear SVG before adding new elements
        svg.selectAll("*").remove();

        // Set up scales and dimensions
        const margin = { top: 40, right: 30, bottom: 60, left: 70 };
        const width = 460 - margin.left - margin.right;
        const height = 400 - margin.top - margin.bottom;

        const x = d3.scaleLinear()
            .domain([0, d3.max(data, d => d.value)])
            .range([0, width]);

        const y = d3.scaleBand()
            .domain(data.map(d => d.name))
            .range([0, height])
            .padding(0.1);

        // Append the svg object to the div
        const svgContent = svg.append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .style("overflow", "visible")
            .append("g")
            .attr("transform", `translate(${margin.left},${margin.top})`);
            

        const tooltipDiv = d3.select(d3Container.current.parentNode)
            .append("div")
            .style("position", "absolute")
            .style("visibility", "hidden")
            .style("padding", "10px")
            .style("background", "var(--primary-color-transparent)")
            .style("border", "1px solid #ddd")
            .style("border-radius", "5px")
            .style("pointer-events", "none")
            .style("text-align", "center")
            .style("color", "var(--text-color)");

        // Draw bars
        svgContent.selectAll("rect")
            .data(data)
            .enter()
            .append("rect")
            .attr("x", 0)
            .attr("y", d => y(d.name))
            .attr("width", d => x(d.value))
            .attr("height", y.bandwidth())
            .attr("fill", "#69b3a2")
            .on("mouseover", (event, d) => {
                tooltipDiv.style("visibility", "visible")
                          .html(`${d.name}, ${xaxis_label}: ${d.value}`)
                          .style("left", (event.pageX + 10) + "px")
                          .style("top", (event.pageY - 20) + "px");
              })
              .on("mousemove", (event) => {
                tooltipDiv.style("left", (event.pageX + 10) + "px")
                          .style("top", (event.pageY - 20) + "px");
              })
              .on("mouseout", () => {
                tooltipDiv.style("visibility", "hidden");
              });

        // Add x-axis
        svgContent.append("g")
        .attr("transform", `translate(0,${height})`)
        .call(d3.axisBottom(x));

        // Add y-axis
        svgContent.append("g")
        .call(d3.axisLeft(y));

        // X-axis label
        svgContent.append("text")
        .style("fill", "var(--text-color)")
        .attr("text-anchor", "end")
        .attr("x", (width + margin.left) / 2)
        .attr("y", height + margin.top)
        .text(xaxis_label);

        // Chart title
        svgContent.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")
        .style("font-size", "20px")
        .style("text-decoration", "underline")
        .style("fill", "var(--text-color)")
        .text(title);
        }
  }, [data, title, xaxis_label]);

  return (
    <div className='flex-div'>
      <div ref={d3Container} />
    </div>
  );
}

export default BarChart;