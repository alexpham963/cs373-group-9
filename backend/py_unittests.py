import FlaskMaster
import unittest

FlaskMaster.app.config["TESTING"] = True
genCli = FlaskMaster.app.test_client()


class Tests(unittest.TestCase):
    def test_foodorg(self):
        with genCli:
            talk = genCli.get("/foodorg")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["foodorg"]
            self.assertEqual(len(data), 12)

    def test_counties(self):
        with genCli:
            talk = genCli.get("/county")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["county"]
            self.assertEqual(len(data), 12)

    def test_groceryStores(self):
        with genCli:
            talk = genCli.get("/grocerystores")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["grocerystores"]
            self.assertEqual(len(data), 12)

    def test_spec_foodorg(self):
        with genCli:
            talk = genCli.get("/foodorg/10")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["foodorg"]
            self.assertEqual(data["zip_code"], 78227)

    def test_spec_foodorg2(self):
        with genCli:
            talk = genCli.get("/foodorg/39")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["foodorg"]
            self.assertEqual(data["address"], "815 S. Ross")

    def test_spec_foodorg3(self):
        with genCli:
            talk = genCli.get("/foodorg/29")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["foodorg"]
            self.assertEqual(data["phone_number"], "(979) 779-3663")

    def test_spec_counties(self):
        with genCli:
            talk = genCli.get("/county/20")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["county"]
            self.assertEqual(data["cost_per_meal"], 2.85)

    def test_spec_counties2(self):
        with genCli:
            talk = genCli.get("/county/88")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["county"]
            self.assertEqual(data["num_people_insecure"], 1020)

    def test_spec_groceryStores(self):
        with genCli:
            talk = genCli.get("/grocerystores/57")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["grocerystores"]
            self.assertEqual(data["name"], "spring market #709")

    def test_spec_groceryStores2(self):
        with genCli:
            talk = genCli.get("/grocerystores/146")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["grocerystores"]
            self.assertEqual(data["city"], "denison")

    def test_search_global(self):
        with genCli:
            talk = genCli.get("/search?terms=arg")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["foodorgdata"]
            self.assertEqual(data[0]["county"], "wilbarger")

    def test_search_foodorg(self):
        with genCli:
            talk = genCli.get("/search/foodorg?terms=Bank")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["length"]
            self.assertEqual(data, 308)

    def test_search_county(self):
        with genCli:
            talk = genCli.get("/search/county?terms=red+river")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["data"]
            self.assertEqual(data[0]["name"], "red river")

    def test_search_grocery(self):
        with genCli:
            talk = genCli.get("/search/grocerystore?terms=red")
            self.assertEqual(talk.status_code, 200)
            data = talk.json["data"]
            self.assertEqual(len(data), 4)

    def test_county_filter(self):
        with genCli:
            talk = genCli.get(
                "/county?filter=num_people_insecure&filterCond=greaterThan&filterNum=70000"
            )
            self.assertEqual(talk.status_code, 200)
            data = talk.json["county"]
            self.assertEqual(len(data), 12)


if __name__ == "__main__":
    unittest.main()
