from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import time


def get_frame(ind):
    banks = open("backend/FoodOrgs/bank_map.txt", "r")
    counter = 0
    for line in banks:
        if counter == ind:
            return line
        counter += 1


# Set up and get web page
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

url = "https://www.google.com/maps"
counties = open("backend/FoodOrgs/CombinedBanksandOrgs2.txt", "r")
county_maps = open("frontend/public/Maps/org_maps2.txt", "w")

counter = 0
valid_lines = []
banks = [
    "East Texas Food Bank",
    "Coastal Bend Food Bank",
    "Houston Food Bank",
    "South Plains Food Bank",
    "Central Texas Food Bank",
    "Tarrant Area Food Bank",
    "Food Bank of the Rio Grande Valley",
    "El Pasoans Fighting Hunger Food Bank",
    "Food Bank of the Golden Crescent",
    "High Plains Food Bank",
    "North Texas Food Bank",
    "San Antonio Food Bank",
    "South Texas Food Bank",
    "Southeast Texas Food Bank",
    "West Texas Food Bank",
    "Wichita Falls Area Food Bank",
    "Food Bank of West Central Texas",
    "Brazos Food Bank",
    "Brazos Valley Food Bank",
    "Andrews Food Bank",
]

for line in counties:

    counter += 1
    split = line.split(",")

    if "Bank" in split[1] and split[1] in banks:
        bank_index = banks.index(split[1])
        frame = get_frame(bank_index)
        county_maps.write(frame.strip())
        county_maps.write("\n")
        valid_lines.append(line)

    else:
        search = split[1] + " " + split[2] + " " + split[3]
        driver.get(url)
        time.sleep(2)

        try:
            search_box = driver.find_element(
                By.XPATH,
                "/html/body/div[1]/div[3]/div[8]/div[3]/div[1]/div[1]/div/div[2]/form/input",
            )
            search_box.send_keys(search)
            search_box.send_keys(Keys.ENTER)

            time.sleep(1)
            share_button = driver.find_element(
                By.XPATH,
                "/html/body/div[1]/div[3]/div[8]/div[9]/div/div/div[1]/div[2]/div/div[1]/div/div/div[4]/div[5]/button",
            )
            share_button.click()

            time.sleep(1)
            embed_button = driver.find_element(
                By.XPATH,
                "/html/body/div[1]/div[3]/div[1]/div/div[2]/div/div[2]/div/div/div/div[2]/button[2]",
            )
            embed_button.click()

            time.sleep(1)
            iframe = driver.find_element(
                By.XPATH,
                "/html/body/div[1]/div[3]/div[1]/div/div[2]/div/div[2]/div/div/div/div[3]/div[1]/input",
            ).get_attribute("value")

            if line not in valid_lines:
                county_maps.write(iframe)
                county_maps.write("\n")
                valid_lines.append(line)

        except NoSuchElementException:
            print(counter, search)
            # county_maps.write("")
            # county_maps.write("\n")
            pass

print(counter)

orgs = open("backend/FoodOrgs/FoodOrg3.txt", "w")
for line in valid_lines:
    orgs.write(line)
