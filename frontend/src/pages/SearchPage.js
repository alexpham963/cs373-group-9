import React from "react";
import CardContainer from "../components/CardContainer";
import "../index.css";
import { useState, useEffect } from "react";
import { useDebounce, getSearchData } from "../utils/utils";
import { useLocation } from "react-router-dom";


const SearchPage = () => {
    const [cardDataList, setCardDataList] = useState([]);

    const [query, setQuery] = useState('');
    const debouncedQuery = useDebounce(query, 500);
    const location = useLocation();

    useEffect(() => {
        if(window.location.href.includes('?query=')) {
            const url = new URL(window.location.href);
            const params = new URLSearchParams(url.search);
            setQuery(params.get('query'))
        }
    }, [location])

    const handleQueryChange = (event) => {
        const newQuery = event.target.value;
        setQuery(newQuery);
    };
    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            handleQueryChange(event);
        }
    }
    useEffect(() => {
        const modelTypes = ["Charity Organizations", "Grocery Stores", "Counties"];
        if (debouncedQuery) {
            setCardDataList([])
            console.log(debouncedQuery)
            for (let modelType of modelTypes) {
                getSearchData(modelType, debouncedQuery).then(modelData => {
                    setCardDataList((currentArray) => [...currentArray, ...modelData])
                });
            }
        } else if (debouncedQuery.length === 0) {
            setCardDataList([])
        }
    }, [debouncedQuery])

    return (
        <div className={`page-background`}>
            <div className="description-container">
                <div className="description-section">
                    <h1>Search</h1>
                    <p>Instances: {cardDataList.length}</p>
                </div>
            </div>
            <div className="searchBar">
                <form>
                    <input type="text" placeholder="Enter Search" value={query} onChange={handleQueryChange} onKeyDown={handleKeyPress}></input>
                </form>
            </div>
            <CardContainer data={cardDataList}></CardContainer>
        </div>
    );
};
export default SearchPage;
