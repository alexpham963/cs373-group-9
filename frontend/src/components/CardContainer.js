import InstanceCard from './InstanceCard';
import '../styles/CardContainer.css';
import { Pagination } from 'react-bootstrap';
import { useState } from 'react';

const CardContainer = ({data: card_data_list}) => {

    let itemsPerPage = 12

    const [currentPage, setCurrentPage] = useState(1);
    const totalPages = Math.ceil(card_data_list.length / itemsPerPage);

    // Get current items
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = card_data_list.slice(indexOfFirstItem, indexOfLastItem);

    // Change page
    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    return (
        <div className='flex-div' style={{flexDirection: 'column', alignItems:'center'}}>
            <div className='card-container'>
                {currentItems.map((cardData, _idx) => {          
                    return <InstanceCard key={cardData.id} ID={cardData.id} cardData={cardData} link={cardData.link}/>
                })}
            </div>
            <Pagination>
                {[...Array(totalPages).keys()].map(number => (
                    <Pagination.Item key={number + 1} active={number + 1 === currentPage} onClick={() => paginate(number + 1)}>
                        {number + 1}
                    </Pagination.Item>
                ))}
            </Pagination>
        </div>
    );
}

export default CardContainer;